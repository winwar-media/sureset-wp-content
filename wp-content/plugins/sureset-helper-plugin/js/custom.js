jQuery(document).ready( function() {

	var hash = window.location.hash;

	if ( hash == '#request-access' ) {
		jQuery('.tabs li').removeClass('active');
		jQuery('.tabs .request-access_tab').addClass('active');
		jQuery('.woocommerce-Tabs-panel').hide();
		jQuery('.woocommerce-Tabs-panel--request-access').show();
	}

	jQuery('.request-access-button').click( function () {
		jQuery( '.tabs li' ).removeClass('active');
		jQuery( '.tabs .request-access_tab').addClass('active');
		jQuery( '.woocommerce-Tabs-panel').hide();
		jQuery( '.woocommerce-Tabs-panel--request-access').show();
	});

	jQuery('.gallery_type_filter').change( function() {
		var gallery_type = "";
		gallery_type = jQuery(this).val();

		if ( gallery_type != 0 ) {
			console.log(gallery_type);

			jQuery.ajax({
				type: 'GET',
				url: document.location.origin + '/wp-json/wp/v2/galleries/?gallery_type=' + gallery_type + '&posts_per_page=100',
				data: { action: 'createHTML' },
				success: function(data) {
					var obj = JSON.stringify(data);
					var jsongallery = jQuery.parseJSON(obj);
					suresetCreateGallery(jsongallery);
				}
			});
		}
	});
});


function suresetCreateGallery(postData) {
	var ourHTMLString = '<div class="inner">';
	for (i = 0; i < postData.length; i++) {
		ourHTMLString += '<article class="feature-post"><a href="' + postData[i].full_image_url + '" data-featherlight="image"><img class="featured" src="' + postData[i].fimg_url + '" alt="' + postData[i].title.rendered + '"><div class="title"><h2>' + postData[i].title.rendered + '</h2></div></a></article>';
	}
	ourHTMLString += '</div>';
	jQuery(".gallery-index").html(ourHTMLString);
}