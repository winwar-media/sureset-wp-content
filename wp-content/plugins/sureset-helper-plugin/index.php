<?php

/**
 * Plugin Name: Sureset - Helper Plugin
 * Description: Helper Plugin to Sureset - helps add a bunch of features to the site
 * Plugin URI:
 * Author: Dwi'n Rhys
 * Author URI: https://dwinrhys.com/
 * Version: 1.0
 * License: GPL2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: Text Domain
 * Domain Path: Domain Path
 * Network: false
 */

defined( 'ABSPATH' ) or exit;

define('SURESET_PLUGIN_PATH',dirname(__FILE__));
define('SURESET_PLUGIN_URL',plugins_url('', __FILE__));

require_once SURESET_PLUGIN_PATH . '/inc/core.php';


/**
 * Add the three roles to the site
 *
 * @return void
 */
function sureset_add_custom_roles_on_plugin_activation() {
    add_role( 'proresin_customer', 'ProResin Customer', get_role( 'customer' )->capabilities );
    add_role( 'approved_installer', 'Approved Installer', get_role( 'customer' )->capabilities );
    add_role( 'bacs_approved', 'BACS Approved', get_role( 'customer' )->capabilities );
} register_activation_hook( __FILE__, 'sureset_add_custom_roles_on_plugin_activation' );