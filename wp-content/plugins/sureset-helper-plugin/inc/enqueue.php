<?php

/**
 * Enqueue the Javascript
 *
 * @return void
 */
function sureset_enqueue_request_access_javascript() {

    wp_enqueue_script( 'request-access-js', SURESET_PLUGIN_URL . '/js/custom.js', array( 'jquery' ), '1.4', true );

} add_action( 'wp_enqueue_scripts', 'sureset_enqueue_request_access_javascript' );



/**
 * Wrapper funciton to load at the end of when all plugins load
 *
 * Done so we can dequeue select2
 *
 * @return void
 */
function shp_plugins_loaded() {
    add_action( 'admin_enqueue_scripts', 'shp_dequeue_select2', 10 );
} add_action( 'plugins_loaded', 'shp_plugins_loaded' );


/**
 * Dequeue's addify Select2 as it conflicts with things
 *
 * @return void
 */
function shp_dequeue_select2() {
    wp_dequeue_script( 'addify_ps-select2-js' );
}
