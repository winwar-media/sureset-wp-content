<?php

/**
 * As we use an hidden field to invalidate the result, we'll invalidate the other field
 * attached to the hidden field.
 * 
 * This is so we can see the result
 *
 * @param  array $validation_result  The validation result.
 * @return array
 */
function sureset_invalidate_another_field($validation_result)
{
    $form = $validation_result['form'];

    //supposing we don't want input 1 to be a value of 86
    if (empty(rgpost('input_50'))) {

        //finding Field with ID of 1 and marking it as failed validation
        foreach ($form['fields'] as &$field) {

            //NOTE: replace 1 with the field you would like to validate
            if ($field->id == '46') {

                // 7 - Check if the field is hidden by GF conditional logic
                $is_hidden = RGFormsModel::is_field_hidden($form, $field, array());

                // 8 - If the field is not on the current page OR if the field is hidden, skip it
                if ($field_page != $current_page || $is_hidden) {
                    continue;
                }

                $field->failed_validation = true;
                $field->validation_message = 'Please Select Some Samples';
                // set the form validation to false
                $validation_result['is_valid'] = false;
                break;
            }
        }
    }

    //Assign modified $form object back to the validation result
    $validation_result['form'] = $form;
    return $validation_result;
}
add_filter('gform_validation_1', 'sureset_invalidate_another_field', 999999);



/**
 * Change the validation message to something else if that field is invalid.
 *
 * @param  string $message  The string we're using
 * @param  array  $form     The form we're looking at
 * @return string           The changed validation message
 */
function sureset_change_validation_message($message, $form)
{
    if (gf_upgrade()->get_submissions_block()) {
        return $message;
    }

    foreach ($form['fields'] as $field) {
        if ($field->failed_validation && $field->id == '46') {
            $message = "<div class='validation_error'>" . esc_html__('Please choose at least one sample to be sent out to you (highlighted below)') . '</div>';
        }
    }

    return $message;
} add_filter('gform_validation_message_1', 'sureset_change_validation_message', 99999, 2);
