<?php

/**
 * This will replace the buy now button on products should the user be allowed to buy a restricted product
 *
 * @return void
 */
function sureset_replace_buynow_button() {

	// Only run on single product pages
	if ( ! is_product() && ! is_shop() && ! is_product_category() ) {
		return;
	}

	if ( is_product() ) {
		$restrictedproduct = get_field( 'restricted_product' );

		// Make sure the Product is restricted
		if ( $restrictedproduct ) {

			if ( ! is_user_logged_in() ) {
				sureset_setup_restricted_products_single();
			} else {
				$current_user               = wp_get_current_user();
				$userviewrestrictedproducts = get_field( 'view_restricted_products', 'user_' . $current_user->ID );

				if ( ! $userviewrestrictedproducts ) {
					sureset_setup_restricted_products_single();
				}
			}
		}
	}

	if ( is_shop() || is_product_category() ) {

		$restrictedproduct = get_field( 'restricted_product' );

		// Make sure the Product is restricted
		if ( $restrictedproduct ) {

			if ( ! is_user_logged_in() ) {
				sureset_setup_restricted_products_loop();
			} else {
				$current_user               = wp_get_current_user();
				$userviewrestrictedproducts = get_field( 'view_restricted_products', 'user_' . $current_user->ID );

				if ( ! $userviewrestrictedproducts ) {
					sureset_setup_restricted_products_loop();
				}
			}
		}
	}

} add_action( 'wp', 'sureset_replace_buynow_button' );


/**
 * Function that adds/removes the various actions from hooks in WordPress in single pages.
 *
 * Useful as it keeps things in one place
 *
 * @return void
 */
function sureset_setup_restricted_products_single() {
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	add_action( 'woocommerce_single_product_summary', 'sureset_request_access_single', 30 );
	add_filter( 'woocommerce_product_tabs', 'sureset_add_request_access_tab', 10 );
}


/**
 * Function that adds/removes the various actions from hooks in WordPress in archive pages.
 *
 * Useful as it keeps things in one place
 *
 * @return void
 */
function sureset_setup_restricted_products_loop() {
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	add_action( 'woocommerce_after_shop_loop_item', 'sureset_request_access_loop', 10 );
}

/**
 * Function for replacing the buy this product with a button instead linking to a contact form
 *
 * @return void
 */
function sureset_request_access_single() {
	?>
	<a href="#request-access" class="single_add_to_cart_button button alt request-access-button"><?php _e( 'Request Access', 'sureset' ); ?></a>
	<?php
}

/**
 * Function for replacing the buy this product with a button instead linking to a contact form
 *
 * @return void
 */
function sureset_request_access_loop() {
	?>
	<a href="<?php echo get_permalink(); ?>" class="button product_type_simple add_to_cart_button request-access-button"><?php _e( 'View & Request Access', 'sureset' ); ?></a>
	<?php
}


/**
 * Filter for adding the tab
 *
 * @param [type] $tabs
 * @return void
 */
function sureset_add_request_access_tab( $tabs ) {
	$tabs['request-access'] = array(
		'title'    => __( 'Request Access', 'woocommerce' ),
		'priority' => 15,
		'callback' => 'sureset_product_request_access_tab',
	);

	return $tabs;
}

/**
 * Content for the "Request Access tab".
 *
 * @return void
 */
function sureset_product_request_access_tab() {
	echo '<a name="request-access"></a>';
	echo gravity_form( 3 );

	if( $_REQUEST['is_submit_3'] ){
		?>
		<script>
		setTimeout(function(){ 
			jQuery(function($){
				$('.wc-tabs li').removeClass('active'); 
				$('.woocommerce-Tabs-panel').css('display', 'none'); 
				$('.request-access_tab').addClass('active'); 
				$('.woocommerce-Tabs-panel--request-access').css('display', 'block'); 
			})
		}, 750);
		</script><?php
	}

} 
