<?php
/**
Plugin Name:  Under2 Header / Footer Inject
Plugin URI:   https://under2.agency/
Description:  Allows you to inject some code into the head or footer. Only works on posts and page post types
Version:      1.1.0
Author:       Shane Jones
Author URI:   https://profiles.wordpress.org/shanejones
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  under2_staging
 */

abstract class U2_Header_Footer {
	public static function add() {
		$screens = ['post', 'page'];
		foreach ($screens as $screen) {
			add_meta_box( 'u2_metaboxes','Header Footer Injection', [self::class, 'html'], $screen );
		}
	}

	public static function save($post_id) {
		if(isset($_POST['u2_header'])){
			update_post_meta( $post_id,'u2_header_key', $_POST['u2_header'] );
		}
		if(isset($_POST['u2_footer'])){
			update_post_meta( $post_id,'u2_footer_key', $_POST['u2_footer'] );
		}
	}

	public static function output_header(){
		$post_id = get_the_ID();
		echo get_post_meta($post_id, 'u2_header_key', true);
	}

	public static function output_footer(){
		$post_id = get_the_ID();
		echo get_post_meta($post_id, 'u2_footer_key', true);
	}

	public static function html($post) {
		$header = get_post_meta($post->ID, 'u2_header_key', true);
		$footer = get_post_meta($post->ID, 'u2_footer_key', true);
		?>

		<style>
			.u2-metabox label {
				display: block;
				font-weight: bold;
				padding-top: 1rem;
			}
			.u2-metabox textarea {
				display: block;
				font-family: monospace;
				padding: 0.5rem;
				line-height: 1.25;
				font-size: 12px;
				width: 100%;
			}
			.u2-metabox p.right {
				text-align: right;
			}
			.u2-metabox img {
				height: 12px;
			}
		</style>

		<div class="u2-metabox">
			<p>The code you add here will output on the page header or footer immediately. Please ensure your code is valid as this could mess things up a little bit.</p>
			<label for="u2_header">Header</label>
			<textarea name="u2_header" id="u2_header" cols="20" rows="10"><?php echo $header ?></textarea>
			<label for="u2_footer">Footer</label>
			<textarea name="u2_footer" id="u2_footer" cols="20" rows="10"><?php echo $footer ?></textarea>
			<p class="right">Powered by <a href="https://under2.agency/" target="_blank"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAAdCAYAAABhXag7AAAHRUlEQVR4Ae1bBYzjOhD9zMzMzHzMzMzMzMzMjFmmro4Zl3l1Cz1mZuZrj8F/5suVrGic1FG2/bTSU5N4Mh77Zcb22PtQYGBgGmBjQECAk2Mz3Hcq173yQ3oEBQW9hzJ6eUB5j8zcuXO/xHJRBt776x5+v6P0BgcH/0DYEKiXmzdv3jqQvQxFF0AGf0/Dbw7Kwm8L+Hkb5VQA70zkbXAqYjO0p66gpxmlJzQ01OlwOJxgt9PTd4B0QAjaPHv27Oe8tRXq+wnfAQwEjAWMBj3d4LcC/D5DvYOGMQIaJYzkSeTbCQ3NJ5FhYMgsieElCfk9BBl7AMwAdwDzAL8rEJwBYFaAHe3Rgx0uk4P2Gem5AGhmYmN1QBbATM94imAXYfhYSUWfSpQ3FD6Cn42MkOgtSMgm6+XArq0KnT9n0aJFj3pB8FKUt4hOgp6+6gTTfYiQfjjm2Dhjxown/UUwQ2+1SjAPbfc57qKcSV0XY2JiXlEkWN2DTQgOCQnxRh/KPa+zrY5Fu2L8RTDCYZVgGM9eh054B4HjraZpH/EhoQcgTu81S5YsibTiwTi+A+IBiXCfZIDqBgSjHjeO0zAGVw0PD8cw2w6eLTTomx4efRh90A4qCkK7a/G2/w5Ik+gq6C+Cb4DxT1gh2AuyygJOANiqVasG4bO0tLTy6enpzRQJXmeh7r4KQ1JHSd+sEmSKS2QKi7oYYw/DsyuEnOYvghl+gXlBMCI+Pv7FlJSUmngNxJbOyMgIA8xLTU39RSFEJ9hBMPfAZyTy24l6s4TybkT5Uck4vYSQTfIXwYg1agSrY8eOHU8AwaMBGhA8AeF0Op/xkuAkHxAcSchvFUgbTdi1QqIrmJDd4jOCcSwC3BOe3ceZnt0E30lPbXdv29Y+jLFH8X7Dhg3vA7GzgOQR3JMr/o0IDiHqTRPKHVSfKHwsm31J8BL0Wt0kqK5dBDOX641bjrBod83yjF2+LIZiDNMNgdgAwCS4HoKe/TcJ0QmE/EJdn3QDdETwhEYpia4kQle8L0O0Rkz5Y+wg+E5udpfrnVpfvZbvK4beqy8XvHg0/sKk6wsvJlkxdhGME0pqNQDltwj5xqr1YiTkCR6mQ4gvCV6B035x7QqpO4ZrVKsEsyOHit4cPzLXVbcSu1amAHP363JWJgvk9gNoAAegtBcefIWvuTMkyAI4vCGY6MN8kozchWHDhj1i4cOaJlkLV/ElwVn8/RUiwatXr+7AnxdWIfh2cuIYDMdIrrt5PeYqW4CxS5eqyOQzMzNrA7FTwIOnwW8VmxIdV80I5h90Bl+rZkG/7jfQl88CuVUlus76OtGxUW8QJiJWrlzpIf53bwm+s39/AVf9qszdpDZzt2nMXJWKs5vazByVjrGBYOyjHRTBijgLeiZAn72sajdu8BjYVsHXBOdiGYYgcexZvHgx4wv1t2mC6dCM3oueiyTjNbt27RujzoAs0qyoqKhQBP5hStAnHmz+kRyH3/44JquQa5LC1FDGLwQjxFRddHQ0g06vjuOzSoi+E7su7FrFYui9DCZZg2VyfCvyTb1unJh4MQanmYzDDusEy73ODJqmtUJ5s0yYPwmu4HkOnsUgT7sc9SpPss4c+56dP/+lhVC23UezaMRV3GLFfXQEXoPuLpKNkqvy3S95dktciqKM3wnmYfoq3z1BD76OjQZcti+TJU8o4DaiD9fBFyX733Ul/VjcQH9DA3LDUca/BBOpNZxoRUZGMn6C46zdBIOOF3UZNETpv0km66xk+1G+X06TO9XILgxhbuKl0ZL4/5GkksaKBBcWwrQgZy/BON7Ta03/EwxlTkJ+DqH3GcoJOUaijBnBF4mKJqh4MIYcFYIRvPEYpvOEYJiZtpV9iCYEY3vynGCoI4sKtYTexebHn+Qgt63wy5cYVUR2SkORYDFM204wHqUh2pTzd9ouhLkHdRZsnv6QnYRcp7d2yRrowvUp4XWhVIW4jlMkWAzTthEcERGBEWYRdcgAhpdXFAiOy2uC4bRJMlHvIpN9XtR5UyWlKV1X4VIBj8Z4Tg1I5BAZKKNKMO+UUyoE8/HoRUxUeIDzAkAtDG8SPfcBvyhmslzCEVcS/IhstFWCYUkUQ9S7lJggUraNBcwxwVSsGyt6Ar8K2TFUTMkBjhsQUdoqwWiEIsGr+BLrigBmgFMY5vLwVOUpqwTD+n+tyZGd0kS5Kl4Tz90yBRCTAnWC8cC7CsHoVQq2LSHCsq0Eoz1WCYbos4LQuV06j1C3zY05brGh7RSVaJIOyyc9fEbLn5boP0TIZnlhV5x52k9ZpwwXBD3jpUdh6eg1SzJhLcLL59jiwcR/LmhoOPUCD+VLxVkztZTC8VtAPDbGKEODMvp38F9KCFkk754HPOV3CLAK7vthRLAwORpC1G8KeC9WbBduAKAesRywFPPekhXJd4Iesd0jub5mFuwS616B/xYjPSXA/w+mFG7x8XxuPnG27A9gyMHNAw/M7fkffwJ4chbAx1WvLgAAAABJRU5ErkJggg==" alt="Under2"></a></p>
		</div>
		<?php
	}
}

add_action('add_meta_boxes', ['U2_Header_Footer', 'add']);
add_action('save_post', ['U2_Header_Footer', 'save']);
add_action('wp_head', ['U2_Header_Footer', 'output_header']);
add_action('wp_footer', ['U2_Header_Footer', 'output_footer']);
