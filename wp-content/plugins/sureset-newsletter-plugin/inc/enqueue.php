<?php

/**
 * Enqueue the stylesheet the right way
 *
 * @return void
 */
function sureset_newsletter_enqueue_scripts() {
    wp_enqueue_style( 'style-name', SURESET_HELPER_URL . '/css/style.css' );
}
add_action( 'wp_enqueue_scripts', 'sureset_newsletter_enqueue_scripts' );