<?php

/*
 Register Newsletter CPT
 */
function sureset_generate_newsletter_cpt() {

	$labels = array(
		'name'                  => _x( 'Newsletters', 'Post Type General Name', 'sureset' ),
		'singular_name'         => _x( 'Newsletter', 'Post Type Singular Name', 'sureset' ),
		'menu_name'             => __( 'Newsletters', 'sureset' ),
		'name_admin_bar'        => __( 'Newsletter', 'sureset' ),
		'archives'              => __( 'Newsletter Archives', 'sureset' ),
		'attributes'            => __( 'Newsletter Attributes', 'sureset' ),
		'parent_item_colon'     => __( 'Parent Newsletter:', 'sureset' ),
		'all_items'             => __( 'All Newsletters', 'sureset' ),
		'add_new_item'          => __( 'Add New Newsletter', 'sureset' ),
		'add_new'               => __( 'Add New', 'sureset' ),
		'new_item'              => __( 'New Item', 'sureset' ),
		'edit_item'             => __( 'Edit Newsletter', 'sureset' ),
		'update_item'           => __( 'Update Newsletter', 'sureset' ),
		'view_item'             => __( 'View Newsletter', 'sureset' ),
		'view_items'            => __( 'View Newsletters', 'sureset' ),
		'search_items'          => __( 'Search Newsletter', 'sureset' ),
		'not_found'             => __( 'Not found', 'sureset' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sureset' ),
		'featured_image'        => __( 'Featured Image', 'sureset' ),
		'set_featured_image'    => __( 'Set featured image', 'sureset' ),
		'remove_featured_image' => __( 'Remove featured image', 'sureset' ),
		'use_featured_image'    => __( 'Use as featured image', 'sureset' ),
		'insert_into_item'      => __( 'Insert into Newsletter', 'sureset' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Newsletter', 'sureset' ),
		'items_list'            => __( 'Newsletters list', 'sureset' ),
		'items_list_navigation' => __( 'Newsletter list navigation', 'sureset' ),
		'filter_items_list'     => __( 'Filter Newsletter list', 'sureset' ),
	);
	$args = array(
		'label'                 => __( 'Newsletter', 'sureset' ),
		'description'           => __( 'Newsletter for Sureset', 'sureset' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'author' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-media-spreadsheet',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
        'exclude_from_search'   => false,
        'rewrite'               => array('slug' => 'newsletter', 'with_front' => false ),
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'newsletter', $args );

}
add_action( 'init', 'sureset_generate_newsletter_cpt', 0 );