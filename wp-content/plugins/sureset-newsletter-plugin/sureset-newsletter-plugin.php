<?php

/**
 * Plugin Name:  Sureset - Newsletter Plugin
 * Plugin URI:
 * Description:  Newsletter plugin for Sureset
 * Version:      0.1
 * Author:       Dwi'n Rhys
 * Author URI:   https://dwinrhys.com/
 * License:      GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:  under2_popups
 */


define( 'SURESET_HELPER_PATH', dirname( __FILE__ ) );
define( 'SURESET_HELPER_URL', plugins_url( '', __FILE__ ) );

require_once SURESET_HELPER_PATH . '/inc/core.php';
