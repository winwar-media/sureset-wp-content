<?php
/**
 * Use Lozad (lazy loading) for attachments/featured images
 */
add_filter('wp_get_attachment_image_attributes', function ($attr, $attachment) {
    // Bail on admin
    if (is_admin()) {
        return $attr;
    }

    if ($attr['src']) {
        $attr['data-src'] = $attr['src'];
        unset($attr['src']);
    }

    if ($attr['srcset']) {
        $attr['data-srcset'] = $attr['srcset'];
        unset($attr['srcset']);
    }

    $attr['class'] .= ' lozad';

    return $attr;
}, 10, 2);


/**
 * Use production URL on images
 * After the whole DB has been search-replaced
 */
add_filter('wp_get_attachment_image_attributes', function ($attr, $attachment) {
    // Bail on admin
    // Or non-debug install
    if (is_admin() || WP_DEBUG === false) {
        return $attr;
    }

    foreach ($attr as $key => $attribute) {
        $domain_dev = '/http:\/\/sureset.test/';
        $domain_prod = 'https://www.sureset.co.uk';

        $attribute = preg_replace($domain_dev, $domain_prod, $attribute);
        $attr[$key] = $attribute;
    }
    
    return $attr;
}, 20, 2);
