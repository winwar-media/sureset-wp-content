<?php
/**
 * Get all attachments on a page
 * Re-map their URLs to match the same naming convention we use for new files
 *
 * /%year%/%monthnum%/%day%/id-%post_id%/%postname%/
 *
 * Settings:
 * https://www.sureset.co.uk/wp-admin/options-general.php?page=custom-upload-dir%2Fcustom_upload_dir.php
 */
add_action('wp_head', function () {
    
    // Only perform on single pages
    if (is_single()) {
        // Get all imagees for this post
        global $post;
        $attachments = get_attached_media('image');
        $i = 0;

        foreach ($attachments as $image) {
            // Only check for images that do not alreay have this URL format
            if (!strpos($image->guid, '/id-')) {
                // Clean current path
                $path_current = get_attached_file($image->ID);
                $path_array = explode('/', $path_current);
                $path_start = join('/', array_splice($path_array, 0, -1)) . '/';
                
                // Clean current URL
                $url_current = explode('/', $image->guid);
    
                $file = end($url_current);
                $url_start = str_replace($file, '', $image->guid);
                $url_day = date('d');
                $url_id = 'id-' . $post->ID;
                $url_postname = $post->post_name;

                $new_end =  $url_day . '/' . $url_id . '/' . $url_postname . '/' . $file;
    
                $path_new = $path_start . $new_end;
                $url_new = $url_start . $new_end;
                
                var_dump('Now: ' . $image->guid);

                // Update and move one image
                // todo: this isn't actually working yet!
                if ($i === 0) {
                    wp_update_attachment_metadata($image->ID, 'guid', $path_new);
                    var_dump('Updated: ' . $image->guid);

                    // Move file to new directory
                    rename($path_current, $path_new);

                    echo 'Moved: ';
                    var_dump($path_new);
                    echo '<br>';
                    var_dump($url_new);
                    echo '<hr>';

                    $i++;
                }

                // Delete old thumbnails

                // Regenerate new thumbnails
            }
        }
    }
});
