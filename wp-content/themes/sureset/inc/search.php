<?php

/**
 * Search
 */
class Search {

	public $postcode;
	public $location;
	public $locations = array();
	public $locations_ordered;
	public $postcode_status;


	public function __construct( $postcode ) {

		$this->postcode = $postcode;

		$this->check_location();

		$posts = $this->get_posts();

		foreach ( $posts->posts as $post ) {

			$latlong = $this->get_lat_longs( $post->ID );

			$distance = number_format( $this->calculate_distance($latlong[0]), 2, '.', '' );

			$result = array(
				'postcode'         => $postcode,
				'post_id'          => $post->ID,
				'distance'         => $distance,
			);

			array_push($this->locations, $result);

		}


		$all = $this->locations;
		$this->array_sort( $all, 'distance' );
		reset($all);

		$this->locations_ordered = array_slice($all, 0, 8);

	}

	/**
	 * Returns the distance in miles between locations
	 *
	 * @param  array $latlong
	 * @return int
	 */
	public function calculate_distance($latlong)
	{
		$lat2 = isset($latlong['lat']) ? $latlong['lat'] : null;
		$lon2 = isset($latlong['lng']) ? $latlong['lng'] : null;

		$lon1 = $this->location['result']['longitude'];
		$lat1 = $this->location['result']['latitude'];

		$theta = $lon1 - $lon2;
		$dist  = sin( deg2rad( $lat1 ) ) * sin( deg2rad( $lat2 ) ) + cos( deg2rad( $lat1 ) ) * cos( deg2rad( $lat2 ) ) * cos( deg2rad( $theta ) );
		$dist  = acos( $dist );
		$dist  = rad2deg( $dist );
		$miles = $dist * 60 * 1.1515;

		return $miles;

	}


	public function check_location() {
		$location_details = $this->get_location();

		if ( $location_details['status'] == 200 ) {
			$this->location = $location_details;
			$this->postcode_status = true;
		} else {
			$this->postcode_status = false;
		}
	}


	public function get_location() {
		$url = 'https://api.postcodes.io/postcodes/' . str_replace( ' ', '', $this->postcode );

		$handle = curl_init($url);
		curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
		$response = curl_exec($handle);

		$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
		if($httpCode == 404) {
			/* Handle 404 here. */
		} else {
			return json_decode($response , true );
		}

		curl_close($handle);
	}


	public function get_posts() {
		$args = array(
			'post_type'      => 'projects',
			'posts_per_page' => -1,
		);

		$posts = new WP_Query( $args );

		return $posts;

	}


	public function get_lat_longs( $post_id ) {
		return get_post_meta( $post_id, 'address', false );
	}


	public function array_sort( &$array, $key ) {
		$sorter = array();
		$ret    = array();
		reset( $array );
		foreach ( $array as $ii => $va ) {
			$sorter[ $ii ] = $va[ $key ];
		}
		asort( $sorter );
		foreach ( $sorter as $ii => $va ) {
			$ret[ $ii ] = $array[ $ii ];
		}
		$array = $ret;
	}


}
