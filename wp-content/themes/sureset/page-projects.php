<?php
/** Template Name: Projects Search */

get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/projects');

get_template_part('template-parts/content');

get_template_part('template-parts/above-footer-blocks');

get_footer();