<?php
/** Template Name: Gallery */

get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/hero-gallery');

get_template_part('template-parts/gallery-type-filter');

get_template_part('template-parts/gallery-loop');

get_template_part('template-parts/above-footer-blocks');

get_footer();