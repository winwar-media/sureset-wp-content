<?php
get_template_part('template-parts/header');

?>
<section class="hero-blank">
	<div class="container">
		<h1>This page can not be found</h1>
	</div>
</section>
<section class="content">
	<div class="container center" style="padding: 0 1rem 5rem 1rem">
		<p>Head <a href="/">back to the homepage</a> or use the menus at the top and bottom of this page to browse somewhere else.</p>
	</div>
</section>
<?

get_footer();