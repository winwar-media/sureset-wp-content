<?php
/** Template Name: Videos Template */
get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/content-videos');

get_footer();