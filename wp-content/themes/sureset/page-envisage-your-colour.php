<?php
/** Template Name: Envisage Template */
get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/hero');

get_template_part('template-parts/envisage-block');

get_footer();