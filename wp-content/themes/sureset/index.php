<?php
get_template_part('template-parts/header');

get_template_part('template-parts/hero-slider-blog-feature');

get_template_part('template-parts/posts-loop');

get_template_part('template-parts/home-newsletter-signup');

get_template_part('template-parts/home-logo-bar');

get_template_part('template-parts/above-footer-blocks');

get_footer();