{
"@type": "Review",
"author": "<?php comment_author(); ?>",
"datePublished": "<?php echo get_comment_date('Y-m-d');?>",
"reviewBody": "<?php echo wp_strip_all_tags( get_comment_text(), true )?>",
"reviewRating": {
  "@type": "Rating",
  "bestRating": "5",
  "ratingValue": "<?php echo $rating = intval( get_comment_meta( $comment->comment_ID, 'rating', true ) ); ?>",
  "worstRating": "1"
}
},