<?php if ( is_active_sidebar( 'sidebar-newsletter' ) ) { ?>
    <aside class="newsletter">
        <ul id="sidebar">
            <?php dynamic_sidebar('sidebar-newsletter'); ?>
        </ul>
    </aside>
<?php } ?>