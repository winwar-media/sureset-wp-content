<?php
/** Template Name: Downloads Template (No Filters) */

get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/content-downloads-no-filters');

get_footer();