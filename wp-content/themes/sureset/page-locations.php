<?php
/** Template Name: Locations */


get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/hero');

get_template_part('template-parts/content');

get_template_part('template-parts/locations-loop');

get_footer();