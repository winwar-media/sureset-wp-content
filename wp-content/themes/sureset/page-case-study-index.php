<?php
/** Template Name: Case Study Index Template */

get_template_part('template-parts/header');

if( has_post_thumbnail() ){
	get_template_part('template-parts/hero');
} else {
	get_template_part('template-parts/hero-blank');
}

get_template_part('template-parts/case-study-filters');

get_template_part('template-parts/case-study-loop');

get_footer();