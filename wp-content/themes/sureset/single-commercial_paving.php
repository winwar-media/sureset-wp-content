<?php

get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/hero');

get_template_part('template-parts/content-offerings');

get_template_part('template-parts/gallery-slider');

get_template_part('template-parts/featured-offerings');

get_template_part('template-parts/above-footer-blocks');

get_footer();