<?php
get_template_part('template-parts/header');

get_template_part('template-parts/hero-slider');

get_template_part('template-parts/home-four-pods');

get_template_part('template-parts/home-content');

get_template_part('template-parts/video-block');

get_template_part('template-parts/home-four-pod-slider-colours');

get_template_part('template-parts/home-brochure-download');

//get_template_part('template-parts/home-app-download');

get_template_part('template-parts/home-testimonials');

get_template_part('template-parts/home-newsletter-signup');

get_template_part('template-parts/home-logo-bar');

get_footer();