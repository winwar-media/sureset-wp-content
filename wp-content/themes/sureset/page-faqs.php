<?php
/** Template Name: FAQ Template */
get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/featured-image');

get_template_part('template-parts/content-faq');

get_footer();