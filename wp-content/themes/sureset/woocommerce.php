<?php get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs-shop');

if ( is_shop() && !is_search() ) {
    get_template_part('template-parts/hero-shop');
}

get_template_part('template-parts/search-shop');

get_template_part('template-parts/woocommerce_content');

if (is_shop()) {
    get_template_part('template-parts/above-footer-blocks');
}


get_footer();