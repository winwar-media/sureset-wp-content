<?php 
/* 
$search = get_search_query() ? get_search_query() : '';


$blog = new WP_Query( array(
	'post_type'        => 'post',
	'posts_per_page'   => -1,
	'orderby'          => 'title',
	'order'            => 'ASC',
	's'                => $search,
) );

$case_studies = new WP_Query( array(
	'post_type'        => 'case_studies',
	'posts_per_page'   => -1,
	'orderby'          => 'date',
	'order'            => 'ASC',
	's'                => $search,
) );

$commercial = new WP_Query( array(
	'post_type'        => 'commercial_paving',
	'posts_per_page'   => -1,
	'orderby'          => 'title',
	'order'            => 'ASC',
	's'                => $search,
) );

$all_pages = new WP_Query( array(
	'post_type'        => 'page',
	'posts_per_page'   => -1,
	'orderby'          => 'title',
	'order'            => 'ASC',
	's'                => $search,
) );

$products = new WP_Query( array(
	'post_type'        => 'product',
	'posts_per_page'   => -1,
	'orderby'          => 'title',
	'order'            => 'ASC',
	's'                => $search,
) );

$gallery = new WP_Query( array(
	'post_type'        => 'galleries',
	'posts_per_page'   => -1,
	'orderby'          => 'title',
	'order'            => 'ASC',
	's'                => $search,
) );

$locations = new WP_Query( array(
	'post_type'        => 'locations',
	'posts_per_page'   => -1,
	'orderby'          => 'title',
	'order'            => 'ASC',
	's'                => $search,
) );

$paving = new WP_Query( array(
	'post_type'        => 'paving_ranges',
	'posts_per_page'   => -1,
	'orderby'          => 'title',
	'order'            => 'ASC',
	's'                => $search,
) );



$residential = new WP_Query( array(
	'post_type'        => 'residential_paving',
	'posts_per_page'   => -1,
	'orderby'          => 'title',
	'order'            => 'ASC',
	's'                => $search,
) );


get_template_part('template-parts/header');

?>

<section class="featured-image">
	<div class="popper"></div>
	<div class="container">
		<h1><?php printf( esc_html__( 'Search Results for: %s', 'sst' ),  get_search_query()  ); ?></h1>
	</div>
</section>


<section class="search-bar">
	<main class="container content">

	<?php get_search_form('true'); ?>

	<?php if ( ! have_posts() ) : ?>

		 <h2>Nothing Found, please search again</h2>

	<?php endif; ?>
	</main>
</section>

<?php wp_reset_postdata(); ?>

<?php // Pages

if ( $all_pages->have_posts() ) : ?>
<section class="paving-loop paving-ranges">
	<h2 class="search-title">Pages <span>( <?php echo $all_pages->post_count; ?> ) </span> </h2>
	<main class="container content search-tab">		
			<?php while ( $all_pages->have_posts() ) : $all_pages->the_post();?>
				<?php get_template_part('template-parts/commercial-loop-article'); ?>
			<?php endwhile; ?>
	</main>
</section>
<?php endif; ?>


<?php wp_reset_postdata(); ?>

<?php // Case Studies

if ( $case_studies->have_posts() ) : ?>
<section class="case-study-loop">
	<h2 class="search-title">Case Studies <span>( <?php echo $case_studies->post_count; ?> ) </span> </h2>
	<main class="container content search-tab">		
			<?php while ( $case_studies->have_posts() ) : $case_studies->the_post();?>
				<?php get_template_part('template-parts/case-study-loop-article'); ?>
			<?php endwhile; ?>
	</main>
</section>
<?php endif; ?>

<?php wp_reset_postdata(); ?>

<?php // Commercail

if ( $commercial->have_posts() ) : ?>
<section class="paving-loop">
	<h2 class="search-title">Commercial Paving <span>( <?php echo $commercial->post_count; ?> ) </span> </h2>
	<main class="container content search-tab">
			<?php while ( $commercial->have_posts() ) : $commercial->the_post();?>
				<?php get_template_part('template-parts/commercial-loop-article'); ?>
			<?php endwhile; ?>
	</main>
</section>
<?php endif; ?>

<?php wp_reset_postdata(); ?>

<?php // Products

if ( $products->have_posts() ) : ?>
<section class="woocommerce">
	<h2 class="search-title">Products <span>( <?php echo $products->post_count; ?> )</span> </h2>
	<main class="container content search-tab">		
		<ul class="products columns-4">
			<?php while ( $products->have_posts() ) : $products->the_post();

			wc_get_template_part( 'content', 'product' );

			 endwhile; ?>
		</ul>
	</main>
</section>
<?php endif; ?>

<?php wp_reset_postdata(); ?>

<?php // Gallery

if ( $gallery->have_posts() ) : ?>
<section class="basic-content ">
	<h2 class="search-title">Gallery <span>( <?php echo $gallery->post_count; ?> )</span></h2>
	<main class="gallery-index container search-tab">
		<div class="inner">
		
			<?php while ( $gallery->have_posts() ) : $gallery->the_post();?>
				<?php get_template_part('template-parts/gallery-loop-article'); ?>
			<?php endwhile; ?>
		</div>
	</main>
</section>
<?php endif; ?>

<?php wp_reset_postdata(); ?>

<?php // Locations

if ( $locations->have_posts() ) : ?>
<section class="paving-loop">
	<h2 class="search-title">Locations <span>( <?php echo $locations->post_count; ?> )</span> </h2>
	<main class="container content search-tab">		
		<?php while ( $locations->have_posts() ) : $locations->the_post();?>
			<?php get_template_part('template-parts/commercial-loop-article'); ?>
		<?php endwhile; ?>
	</main>
</section>
<?php endif; ?>

<?php wp_reset_postdata(); ?>

<?php // Locations

if ( $paving->have_posts() ) : ?>
<section class="paving-loop paving-ranges">
	<h2 class="search-title">Paving ranges <span>( <?php echo $paving->post_count; ?> )</span> </h2>
	<main class="container content search-tab">		
		<?php while ( $paving->have_posts() ) : $paving->the_post();?>
			<?php get_template_part('template-parts/commercial-loop-article'); ?>
		<?php endwhile; ?>
	</main>
</section>
<?php endif; ?>

<?php wp_reset_postdata(); ?>

<?php // Blog

if ( $blog->have_posts() ) : ?>
<section class="paving-loop paving-ranges">
	<h2 class="search-title">News <span>( <?php echo $blog->post_count; ?> )</span> </h2>
	<main class="container content search-tab">		
			<?php while ( $blog->have_posts() ) : $blog->the_post();?>
				<?php get_template_part('template-parts/commercial-loop-article'); ?>
			<?php endwhile; ?>
	</main>
</section>
<?php endif; ?>

<?php wp_reset_postdata(); ?>

<?php // Residential Paving

if ( $residential->have_posts() ) : ?>
<section class="paving-loop">
	<h2 class="search-title">Residential Paving <span>( <?php echo $residential->post_count; ?> )</span> </h2>
	<main class="container content search-tab">		
			<?php while ( $residential->have_posts() ) : $residential->the_post();?>
				<?php get_template_part('template-parts/residential-loop-article'); ?>
			<?php endwhile; ?>
	</main>
</section>
<?php endif; ?>

<?php wp_reset_postdata(); ?>

<?php get_footer(); ?>
 */