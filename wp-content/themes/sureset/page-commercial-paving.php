<?php
/** Template Name: Commercial Paving */


get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/hero');

get_template_part('template-parts/commercial-loop');

get_template_part('template-parts/content');

get_template_part('template-parts/team-commercial');

get_footer();