<?php
/** Template Name: Paving Ranges Template */
get_template_part('template-parts/header');

get_template_part('template-parts/featured-image');

get_template_part('template-parts/content'); 

get_template_part('template-parts/paving-ranges-loop');

get_template_part('template-parts/above-footer-blocks');

get_footer();