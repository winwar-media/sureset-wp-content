<?php
get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/newsletter-loop');

get_template_part('template-parts/home-newsletter-signup');

get_template_part('template-parts/home-logo-bar');

get_footer();