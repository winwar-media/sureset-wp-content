<?php
/** Template Name: Sitemap */

get_template_part('template-parts/header');

get_template_part('template-parts/featured-image');

get_template_part('template-parts/content-sitemap');

get_footer();