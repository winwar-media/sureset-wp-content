<?php 

if ( array_key_exists( 'search_page', $_GET ) ) {
	get_template_part( 'search-shop' );
} else {
	get_template_part( 'search-standard' );
}
