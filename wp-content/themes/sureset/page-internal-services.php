<?php
/** Template Name: Internal Services Template */
get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/hero');

get_template_part('template-parts/process');

get_template_part('template-parts/video-block');

get_template_part('template-parts/services-layout');

get_footer();