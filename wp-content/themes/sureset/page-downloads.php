<?php
/** Template Name: Downloads Template */

get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/content-downloads');

get_footer();