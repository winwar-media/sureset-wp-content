<?php

$search = get_search_query() ? get_search_query() : '';

?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text">Search for:</span>
		<input type="search" class="search-field" placeholder="Search …" value="<?php echo $search; ?>" name="s">
	</label>
	
</form>
