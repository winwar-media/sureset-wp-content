<?php
/** Template Name: Residential Paving */


get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/hero');

get_template_part('template-parts/diy-loop');

get_template_part('template-parts/content');

get_template_part('template-parts/team-diy');

get_footer();