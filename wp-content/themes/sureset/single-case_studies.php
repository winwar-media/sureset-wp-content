<?php
get_template_part('template-parts/header');

get_template_part('template-parts/hero');

$add_cats = isset($_GET['add_cats']) ? $_GET['add_cats'] : '';

if ($add_cats) {
    get_template_part('template-parts/breadcrumbs-single-case-study');
} else {
    get_template_part('template-parts/breadcrumbs');
}


get_template_part('template-parts/content-case-study');

get_template_part('template-parts/process');

if(get_field( 'show_video_block' )){
	get_template_part('template-parts/video-block');
}

get_template_part('template-parts/gallery');

get_footer();