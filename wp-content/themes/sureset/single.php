<?php
get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/hero-blog');

get_template_part('template-parts/author-block');

get_template_part('template-parts/content-blog-post');

get_template_part('template-parts/gallery');

get_footer();