<?php
/** Template Name: Product Ranges Template */
get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/hero-blank');

get_template_part('template-parts/center-button');

get_template_part('template-parts/content-product-range');

get_template_part('template-parts/home-four-pod-slider-colours');

get_template_part('template-parts/home-four-pod-slider-products');

get_footer();