<?php
/** Template Name: Our Services Template */
get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/hero');

get_template_part('template-parts/our-services');

get_template_part('template-parts/above-footer-blocks');

get_footer();