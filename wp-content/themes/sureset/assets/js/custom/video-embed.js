jQuery(function($){

	$('a[data-video]').click(function (e) {
		e.preventDefault();
		var video = $(this).attr('data-video');
		var playlist = $(this).attr('data-playlist');

		if( playlist === 'true' ){
			var autoplay = '&autoplay=1';
		}else{
			var autoplay = '?autoplay=1';
		}

		console.log(playlist);

		$('body').append('<div class="video-embed"><a href="#" class="close">&times;</a><div class="inner"><iframe width="1920" height="753" src="'+ video + autoplay + '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div></div>');

		console.log('ok')

		setTimeout(function(){
			$('.video-embed').addClass('visible')
		},50)
	});

	$(document).on('click', '.video-embed a.close', function (e) {
		e.preventDefault();

		$('.video-embed').fadeOut(250, function(){
			setTimeout(function(){
				$('.video-embed').remove()
			},500)
		})

	})

});