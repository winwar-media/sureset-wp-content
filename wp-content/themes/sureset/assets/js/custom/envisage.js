jQuery( function( $ ) {

	$( '.envisage-for' ).slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: true,
		asNavFor: '.envisage-nav, .envisage-title',
		adaptiveHeight: true
	});
	$( '.envisage-title' ).slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.envisage-nav, .envisage-for'
	});
	$( '.envisage-nav' ).slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		asNavFor: '.envisage-for, .envisage-title',
		dots: false,
		arrows: false,
		centerMode: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true
				}
			} ]
	});

});
