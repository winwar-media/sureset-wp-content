jQuery(document).ready(function($){
	$('.feather-gallery').featherlightGallery({

		gallery: {
			fadeIn: 300,
			fadeOut: 300,
			previousIcon: '«',
			nextIcon: '»',
		},
		openSpeed:    300,
		closeSpeed:   300
	});
});