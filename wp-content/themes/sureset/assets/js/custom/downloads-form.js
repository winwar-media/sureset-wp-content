jQuery(function($){
	$('a.download-form').click(function (e) {
		e.preventDefault();
		var file_name = $(this).attr('data-title');
		var file_url = $(this).attr('data-file');

		$('#input_2_1').val(file_name);
		$('.downloads-form-overlay h3').text('Download - ' + file_name);
		$('#input_2_2').val(file_url);

		$('.downloads-form-overlay').fadeIn();

	});

	$('.downloads-form-overlay a').click(function (e) {
		e.preventDefault();
		$('.downloads-form-overlay').fadeOut()
	})
});