jQuery(function($){

	$('.faq-item .question').click(function () {
		if($(this).parent().hasClass('active')){
			$(this).parent().removeClass('active').find('.answer').slideUp();

		} else {

			$(this).parent().removeClass('active');
			$('.faq-item .answer').slideUp();
			$(this).parent().addClass('active').find('.answer').slideDown()

		}
	});

});