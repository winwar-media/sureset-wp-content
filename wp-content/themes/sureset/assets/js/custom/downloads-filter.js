jQuery(function($){

	$('.download-filters a').click(function (e) {
		e.preventDefault();
		var selection = $(this);
		var download = selection.attr('data-type');
		var current = $(this).parent().find('.active').attr('data-type');

		if(current != download){
			$('.download-filters a').removeClass('active');
			selection.addClass('active');

			if(download == 'all'){
				$('.download-item').slideDown();
			} else {
				$('.download-item').each(function(){
					var d = $(this).attr('data-type');

					if(d.indexOf(download) != -1){
						$(this).slideDown();
					} else {
						$(this).slideUp();
					}
				});
			}
		}
	})
});