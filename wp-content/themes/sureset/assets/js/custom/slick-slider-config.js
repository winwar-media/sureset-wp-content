jQuery(function($){

	$('.hero-slider .slider-items').slick({
		infinite: true,
		arrows: false,
		dots: true,
		autoplay: true
	});

	$('.hero-slider-blog-featured .slider-items').slick({
		infinite: true,
		arrows: true,
		dots: false
	});

	$('.gallery-slider .slider-items').slick({
		infinite: true,
		arrows: true,
		dots: false,
		autoplay: true
	});

	$('.slider.slider-four').slick({
		slidesToShow: 4,
		infinite: true,
		responsive: [{
			breakpoint: 960,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 640,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
			}
		}]
	});


	$('.slider.slider-one').slick({
		slidesToShow: 1,
		infinite: true,
		adaptiveHeight: true
	})


	$('.envisage-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: true,
		asNavFor: '.envisage-nav, .envisage-title'
	});
	$('.envisage-title').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.envisage-nav'
	});
	$('.envisage-nav').slick({
		slidesToShow: 6,
		slidesToScroll: 1,
		asNavFor: '.envisage-for, .envisage-title',
		dots: false,
		arrows: false,
		centerMode: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
				}
			}]
	});



	$('.products-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.products-nav'
	});
	$('.products-nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.products-for',
		dots: false,
		arrows: true,
		centerMode: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
				}
			}]
	});

	$('.gallery-for').slick({
		asNavFor: '.gallery-nav'
	});
	$('.gallery-nav').slick({
	  slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.gallery-for',
		dots: false,
		arrows: true,
		centerMode: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					arrows: false,
				}
			}]
	});
	$('.process-for').slick({
		asNavFor: '.process-nav'
	});
	$('.process-nav').slick({
	  slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.process-for',
		dots: false,
		arrows: false,
		centerMode: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					arrows: false,
				}
			}]
	});


});