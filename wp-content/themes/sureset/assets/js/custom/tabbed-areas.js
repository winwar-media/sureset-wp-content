jQuery(function($){

	$('.tabbed-content .nav li').click(function(){
		var content = $(this).closest('.tabbed-content').find('.content');
		var indicators = $(this).closest('ul').find('li');
		var item = $(this).index();

		indicators.removeClass('active');
		$(this).addClass('active');

		content.removeClass('active');
		content.eq(item).addClass('active')
	});

});