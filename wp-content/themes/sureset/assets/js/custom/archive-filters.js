jQuery(function($){
	$('.archive-filter .upper-tabs a').click(function (e) {
		e.preventDefault();
		var tab = $(this).attr('data-tab');

		$('.archive-filter a').removeClass('active');
		$(this).addClass('active');

		$('.lower-tabs .tab-item').slideUp();

		console

		if(tab != 'all'){
			$('.lower-tabs .' + tab).slideDown();
		} else {
			$('.case-study-loop article').fadeIn();
		}

	})

	$('.archive-filter .lower-tabs .sector a').click(function (e) {
		e.preventDefault();
		var selection = $(this);
		var sector = selection.attr('data-sector');
		var current = $(this).parent().find('.active').attr('data-sector');




		if(current != sector){
			$('.lower-tabs a').removeClass('active');
			selection.addClass('active');

			if(sector == 'all'){
				$('.case-study-loop article').fadeOut();
			} else {
				$('.case-study-loop article').each(function(){
					var t = $(this).attr('data-sector');

					if(t.indexOf(sector) != -1){
						$(this).fadeIn();
					} else {
						$(this).fadeOut();
					}

				});
			}
		}
	})

	$('.archive-filter .lower-tabs .service a').click(function (e) {
		e.preventDefault();
		var selection = $(this);
		var service = selection.attr('data-service');
		var current = $(this).parent().find('.active').attr('data-service');




		if(current != service){
			$('.lower-tabs a').removeClass('active');
			selection.addClass('active');

			if(service == 'all'){
				$('.case-study-loop article').fadeOut();
			} else {
				$('.case-study-loop article').each(function(){
					var t = $(this).attr('data-service');

					if(t.indexOf(service) != -1){
						$(this).fadeIn();
					} else {
						$(this).fadeOut();
					}

				});
			}
		}
	})

});