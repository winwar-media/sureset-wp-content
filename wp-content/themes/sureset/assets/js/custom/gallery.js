jQuery(function($){
	$('a[data-full-size]').click(function (e) {
		e.preventDefault();
		var full = $(this).attr('data-full-size');

		console.log(full);

		$('body')
			.css('overflow', 'hidden')
			.append('<section class="gallery-large"><a href="#" class="close">&times;</a><div class="image" style="background-image: url(' + full + ')"></div></section>')
			.find('.gallery-large').fadeIn();

	});

	$('body').on('click', '.gallery-large a.close', function (e) {
		e.preventDefault();

		$('body')
			.css('overflow', 'auto')
			.find('.gallery-large').fadeOut(500, function(e){
				setTimeout(function(){
					$('body').find('.gallery-large').remove();
				}, 250)
		})

	})
});