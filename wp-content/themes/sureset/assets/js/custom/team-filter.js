jQuery(function($){

	$('.team-filters a').click(function (e) {
		e.preventDefault();
		var selection = $(this);
		var team = selection.attr('data-team');
		var current = $(this).parent().find('.active').attr('data-team');

		if(current != team){
			$('.team-filters a').removeClass('active');
			selection.addClass('active');

			if(team == 'all'){
				$('.team-item').slideDown();
			} else {
				$('.team-item').each(function(){
					var t = $(this).attr('data-team');

					if(t.indexOf(team) != -1){
						$(this).slideDown();
					} else {
						$(this).slideUp();
					}

				});
			}
		}
	})

});