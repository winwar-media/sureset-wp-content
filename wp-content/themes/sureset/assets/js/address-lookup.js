jQuery(function($){
	var timer;

	function get_addresses (){
		var input = $('#input_1_7').val();
		input = input.toLowerCase();
		input = input.replace(/ /g,'');

		$.post('/wp-content/themes/sureset/inc/getaddress_io.php', { postcode: input }, function(data){

			var html = '<div class="address-selection"><a href="#" class="close">&times;</a><div class="inner">' +
				'<style>li.gfield.address_finder {\n' +
				'    position: relative;\n' +
				'}' +
				'.address-selection {\n' +
				'    position: absolute;\n' +
				'    padding:  1rem;\n' +
				'    background-color: white;\n' +
				'    box-shadow: 0 0 5px;\n' +
				'    width: 40%;\n' +
				'}\n' +
				'.address-selection a.close {\n' +
				'    position: absolute;\n' +
				'    right: 0.25rem;\n' +
				'    top: 0.25rem;\n' +
				'}' +
				'.address-selection .inner {\n' +
				'    height: 200px;\n' +
				'    overflow: auto;\n' +
				'}\n' +
				'.address-selection .inner a {\n' +
				'    display: block;\n' +
				'    padding: 0.25rem;' +
				'}' +
				'.address-selection .inner a:hover {\n' +
				'    background-color: #efefef;\n' +
				'}</style>';

				$.each(data.addresses, function(key, value) {
					values = value.split(',');

					var address_1;
					var address_2;

					var full_address = values[0].trim() + ', ';

						if(values[1].trim() != ''){
							full_address += values[1].trim() + ', ';
							address_1 = values[0].trim() + ', '  + values[1].trim() + ', ';
							address_2 = values[2].trim();
						} else {
							address_1 = values[0].trim();
							address_2 = values[1].trim();
						}

						full_address += values[5].trim();

					html +='<a href="#" data-address-1="' + address_1 + '" data-address-2="' + address_2 + '" data-city="' + values[5].trim() + '" data-county="' + values[6].trim() + '">' + full_address + '</a>';

				});

				html +='<a href="#" class="not-found">-- Manually type address --</a></div></div>';

			$('#field_1_7').append(html);

		}, 'json');
	}

	$("#input_1_7").bind('keyup', function(){
		clearTimeout(timer);
		timer = setTimeout(get_addresses, 200)
	});

	$('body').on('click', '.address-selection .close', function(){
		$('.address-selection').remove()
	});

	$('body').on('click', '.address-selection .inner a', function(e){
		e.preventDefault();

		var address_1 = $(this).attr('data-address-1');
		var address_2 = $(this).attr('data-address-2');
		var town_city = $(this).attr('data-city');
		var county = $(this).attr('data-county');

		if(address_1 != ''){
			$('li.gfield.address_1 input').val( address_1 );

		}
		if(address_2 != '') {
			$('li.gfield.address_2 input').val(address_2);
		}
		if(town_city != '') {
			$('li.gfield.town-city input').val(town_city);
		}
		if(county != '') {
			$('li.gfield.county input').val(county);
		}

		$('.address-selection').remove();
	});

});