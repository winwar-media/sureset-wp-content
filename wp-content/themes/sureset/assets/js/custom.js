"use strict";

jQuery(function ($) {
  $('.archive-filter .upper-tabs a').click(function (e) {
    e.preventDefault();
    var tab = $(this).attr('data-tab');
    $('.archive-filter a').removeClass('active');
    $(this).addClass('active');
    $('.lower-tabs .tab-item').slideUp();
    console;

    if (tab != 'all') {
      $('.lower-tabs .' + tab).slideDown();
    } else {
      $('.case-study-loop article').fadeIn();
    }
  });
  $('.archive-filter .lower-tabs .sector a').click(function (e) {
    e.preventDefault();
    var selection = $(this);
    var sector = selection.attr('data-sector');
    var current = $(this).parent().find('.active').attr('data-sector');

    if (current != sector) {
      $('.lower-tabs a').removeClass('active');
      selection.addClass('active');

      if (sector == 'all') {
        $('.case-study-loop article').fadeOut();
      } else {
        $('.case-study-loop article').each(function () {
          var t = $(this).attr('data-sector');

          if (t.indexOf(sector) != -1) {
            $(this).fadeIn();
          } else {
            $(this).fadeOut();
          }
        });
      }
    }
  });
  $('.archive-filter .lower-tabs .service a').click(function (e) {
    e.preventDefault();
    var selection = $(this);
    var service = selection.attr('data-service');
    var current = $(this).parent().find('.active').attr('data-service');

    if (current != service) {
      $('.lower-tabs a').removeClass('active');
      selection.addClass('active');

      if (service == 'all') {
        $('.case-study-loop article').fadeOut();
      } else {
        $('.case-study-loop article').each(function () {
          var t = $(this).attr('data-service');

          if (t.indexOf(service) != -1) {
            $(this).fadeIn();
          } else {
            $(this).fadeOut();
          }
        });
      }
    }
  });
});
"use strict";

jQuery(function ($) {
  var dropdown = $('.categories .dropdown');

  if (dropdown.length) {
    dropdown.click(function () {
      dropdown.find('.dropdown-content').slideToggle();
    });
  }
});
"use strict";

jQuery(function ($) {
  $('.download-filters a').click(function (e) {
    e.preventDefault();
    var selection = $(this);
    var download = selection.attr('data-type');
    var current = $(this).parent().find('.active').attr('data-type');

    if (current != download) {
      $('.download-filters a').removeClass('active');
      selection.addClass('active');

      if (download == 'all') {
        $('.download-item').slideDown();
      } else {
        $('.download-item').each(function () {
          var d = $(this).attr('data-type');

          if (d.indexOf(download) != -1) {
            $(this).slideDown();
          } else {
            $(this).slideUp();
          }
        });
      }
    }
  });
});
"use strict";

jQuery(function ($) {
  $('a.download-form').click(function (e) {
    e.preventDefault();
    var file_name = $(this).attr('data-title');
    var file_url = $(this).attr('data-file');
    $('#input_2_1').val(file_name);
    $('.downloads-form-overlay h3').text('Download - ' + file_name);
    $('#input_2_2').val(file_url);
    $('.downloads-form-overlay').fadeIn();
  });
  $('.downloads-form-overlay a').click(function (e) {
    e.preventDefault();
    $('.downloads-form-overlay').fadeOut();
  });
});
"use strict";

jQuery(function ($) {
  $('.envisage-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.envisage-nav, .envisage-title',
    adaptiveHeight: true
  });
  $('.envisage-title').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.envisage-nav, .envisage-for'
  });
  $('.envisage-nav').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.envisage-for, .envisage-title',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 800,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true
      }
    }]
  });
});
"use strict";

jQuery(function ($) {
  $('.faq-item .question').click(function () {
    if ($(this).parent().hasClass('active')) {
      $(this).parent().removeClass('active').find('.answer').slideUp();
    } else {
      $(this).parent().removeClass('active');
      $('.faq-item .answer').slideUp();
      $(this).parent().addClass('active').find('.answer').slideDown();
    }
  });
});
"use strict";

jQuery(document).ready(function ($) {
  $('.feather-gallery').featherlightGallery({
    gallery: {
      fadeIn: 300,
      fadeOut: 300,
      previousIcon: '«',
      nextIcon: '»'
    },
    openSpeed: 300,
    closeSpeed: 300
  });
});
"use strict";

jQuery(function ($) {
  $('a[data-full-size]').click(function (e) {
    e.preventDefault();
    var full = $(this).attr('data-full-size');
    console.log(full);
    $('body').css('overflow', 'hidden').append('<section class="gallery-large"><a href="#" class="close">&times;</a><div class="image" style="background-image: url(' + full + ')"></div></section>').find('.gallery-large').fadeIn();
  });
  $('body').on('click', '.gallery-large a.close', function (e) {
    e.preventDefault();
    $('body').css('overflow', 'auto').find('.gallery-large').fadeOut(500, function (e) {
      setTimeout(function () {
        $('body').find('.gallery-large').remove();
      }, 250);
    });
  });
});
"use strict";

jQuery(function ($) {
  var observer = lozad(".lozad");
  observer.observe();
});
"use strict";

jQuery(function ($) {
  jQuery(function ($) {
    $('.burger').click(function () {
      $(".navigation nav").css('display', 'flex');
      $(".navigation nav").animate({
        opacity: 1
      }, 200, function () {});
    });
    $('.close').click(function () {
      $(".navigation nav").animate({
        opacity: 0
      }, 300, function () {
        $(".navigation nav").css('display', 'none');
      });
    });
  });
});
"use strict";

jQuery(function ($) {
  $('a.show-menu').click(function (e) {
    e.preventDefault();
    $('body').toggleClass('menu-active');
    $(this).toggleClass('active');
    $('.menu-main-menu-container').toggleClass('active');
  });
});
"use strict";

jQuery(function ($) {
  $('h2.search-title').click(function () {
    $(this).siblings('.search-tab').toggleClass('show');
  });
});
"use strict";

jQuery(function ($) {
  $('.hero-slider .slider-items').slick({
    infinite: true,
    arrows: false,
    dots: true,
    autoplay: true
  });
  $('.hero-slider-blog-featured .slider-items').slick({
    infinite: true,
    arrows: true,
    dots: false
  });
  $('.gallery-slider .slider-items').slick({
    infinite: true,
    arrows: true,
    dots: false,
    autoplay: true
  });
  $('.slider.slider-four').slick({
    slidesToShow: 4,
    infinite: true,
    responsive: [{
      breakpoint: 960,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 640,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }]
  });
  $('.slider.slider-one').slick({
    slidesToShow: 1,
    infinite: true,
    adaptiveHeight: true
  });
  $('.envisage-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.envisage-nav, .envisage-title'
  });
  $('.envisage-title').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.envisage-nav'
  });
  $('.envisage-nav').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.envisage-for, .envisage-title',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 800,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true
      }
    }]
  });
  $('.products-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.products-nav'
  });
  $('.products-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.products-for',
    dots: false,
    arrows: true,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true
      }
    }]
  });
  $('.gallery-for').slick({
    asNavFor: '.gallery-nav'
  });
  $('.gallery-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.gallery-for',
    dots: false,
    arrows: true,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        arrows: false
      }
    }]
  });
  $('.process-for').slick({
    asNavFor: '.process-nav'
  });
  $('.process-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.process-for',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        arrows: false
      }
    }]
  });
});
"use strict";

jQuery(function ($) {
  $('.tabbed-content .nav li').click(function () {
    var content = $(this).closest('.tabbed-content').find('.content');
    var indicators = $(this).closest('ul').find('li');
    var item = $(this).index();
    indicators.removeClass('active');
    $(this).addClass('active');
    content.removeClass('active');
    content.eq(item).addClass('active');
  });
});
"use strict";

jQuery(function ($) {
  $('.team-filters a').click(function (e) {
    e.preventDefault();
    var selection = $(this);
    var team = selection.attr('data-team');
    var current = $(this).parent().find('.active').attr('data-team');

    if (current != team) {
      $('.team-filters a').removeClass('active');
      selection.addClass('active');

      if (team == 'all') {
        $('.team-item').slideDown();
      } else {
        $('.team-item').each(function () {
          var t = $(this).attr('data-team');

          if (t.indexOf(team) != -1) {
            $(this).slideDown();
          } else {
            $(this).slideUp();
          }
        });
      }
    }
  });
});
"use strict";

jQuery(function ($) {
  $("a[href='#top']").click(function () {
    $("html, body").animate({
      scrollTop: 0
    }, "slow");
    return false;
  });
});
"use strict";

jQuery(function ($) {
  $('a[data-video]').click(function (e) {
    e.preventDefault();
    var video = $(this).attr('data-video');
    var playlist = $(this).attr('data-playlist');

    if (playlist === 'true') {
      var autoplay = '&autoplay=1';
    } else {
      var autoplay = '?autoplay=1';
    }

    console.log(playlist);
    $('body').append('<div class="video-embed"><a href="#" class="close">&times;</a><div class="inner"><iframe width="1920" height="753" src="' + video + autoplay + '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div></div>');
    console.log('ok');
    setTimeout(function () {
      $('.video-embed').addClass('visible');
    }, 50);
  });
  $(document).on('click', '.video-embed a.close', function (e) {
    e.preventDefault();
    $('.video-embed').fadeOut(250, function () {
      setTimeout(function () {
        $('.video-embed').remove();
      }, 500);
    });
  });
});

jQuery(function ($) {
    $('.button-read-more').click(function (e) {
        e.preventDefault();
        $(this).hide();
        $(this).closest('.content').find('.panel-read-more').show();
    });
});