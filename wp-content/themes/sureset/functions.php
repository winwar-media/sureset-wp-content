<?php
$theme_data = wp_get_theme();
define('THEME_VER', $theme_data['Version']);
define('THEME_DIR', get_template_directory());
define('THEME_URI', get_template_directory_uri());
define('THEME_NAME', 'printed-editions');

add_theme_support('title-tag');
add_theme_support('post-thumbnails');

add_image_size('full-slider', 1400, 500, true);
add_image_size('medium-square', 200, 200, true);
add_image_size('larger-square', 400, 400, true);
add_image_size('blog-sidebar', 640, 480, true);
add_image_size('case-study-pod', 325, 228, true);


/**
 * Registering the main and the footer menus ready for links to be added
 *
 */
function sst_register_menus()
{
    add_theme_support('woocommerce');

    if (function_exists('register_nav_menu')) {
        register_nav_menu('main-menu', __('Main Menu'));
    }
} add_action('after_setup_theme', 'sst_register_menus');


/**
 * Includes
 */
include 'inc/search.php';
include 'inc/lazyload.php';


/**
 * Add 'defer' to certain scripts, dependant on their handle
 */
function fdFS2d_theme_make_script_defer($tag, $handle, $src)
{


    $script_handles = array(
        'wplc-server-script',
        'wplc-user-script',
        'wplc-md5',
        'wplc-node-server-script',
        'wplc-user-js-emoji-concat',
        'wplc-user-events-script',
        'wplc-theme-modern',
        'wplc-user-pro-editor',
        'wplc-user-pro-events-script',
        'wplc-user-pro-features',
        'wplc-user-jquery-cookie',
        'bleeper-action-script',
    );
    if (in_array($handle, $script_handles)) {
        return str_replace('<script', '<script defer', $tag);
    }

    // Just return the default tag if we didnt get a match above
    return $tag;
}
add_filter('script_loader_tag', 'fdFS2d_theme_make_script_defer', 10, 3);


/**
 * Registering fonts, styles and scripts
 *
 */
function sst_styles_scripts()
{
    wp_enqueue_script('polyfill', 'https://polyfill.io/v3/polyfill.js?features=IntersectionObserver-polyfill', array(), THEME_VER, true);

    if (WP_DEBUG) {
        /** use un minified */
        wp_enqueue_style('main-css', THEME_URI . '/assets/css/styles.css', false, THEME_VER);

        wp_enqueue_script('vendor-scripts', THEME_URI . '/assets/js/vendors.js', array('jquery'), THEME_VER, true);
        wp_enqueue_script('custom-scripts', THEME_URI . '/assets/js/custom.js', array('jquery'), THEME_VER, true);
    } else {
        /** use minified for live */
        wp_enqueue_style('main-css', THEME_URI . '/assets/css/styles.min.css', false, THEME_VER);

        wp_enqueue_script('vendor-scripts', THEME_URI . '/assets/js/vendors.min.js', array(), THEME_VER, true);
        wp_enqueue_script('custom-scripts', THEME_URI . '/assets/js/custom.min.js', array(), THEME_VER, true);
    }

    wp_enqueue_style('extra-css', THEME_URI . '/assets/css/extra.css', false, THEME_VER);

    // Use minified for JS/CSS libraries
    wp_enqueue_script('featherlight-js', THEME_URI . '/assets/js/featherlight.min.js', array( 'jquery' ), '', true);
    wp_enqueue_script('featherlightgallery-js', THEME_URI . '/assets/js/featherlight.gallery.min.js', array( 'jquery' ), '', true);
    wp_enqueue_style('featherlight-css', THEME_URI . '/assets/css/featherlight.min.css', false);
    wp_enqueue_style('featherlightgallery-css', THEME_URI . '/assets/css/featherlight.gallery.min.css', false);
} add_action('wp_enqueue_scripts', 'sst_styles_scripts');


add_action('gform_enqueue_scripts_4', 'address_lookup', 10, 2);
add_action('gform_enqueue_scripts_1', 'address_lookup', 10, 2);
function address_lookup($form, $is_ajax)
{
        wp_enqueue_script('address_lookup', get_template_directory_uri() . '/assets/js/address-lookup.js', array(), '', true);
}



/**
 * Shortcut functions for images
 *
 */
function theme_images($file)
{
    if (WP_DEBUG) {
        echo get_stylesheet_directory_uri() . '/assets/img/' . $file;
    } else {
        echo image(get_stylesheet_directory_uri() . '/assets/img/' . $file);
    }
}


function theme_svg($file)
{
    echo get_stylesheet_directory_uri() . '/assets/img/' . $file;
}


function image($file)
{
    if (WP_DEBUG) {
        echo $file;
    } else {
        $type = pathinfo($file, PATHINFO_EXTENSION);
        $data = file_get_contents($file);
        echo 'data:image/' . $type . ';base64,' . base64_encode($data);
        //echo $file;
    }
}

function get_image($file)
{
    if (WP_DEBUG) {
        return $file;
    } else {
        $type = pathinfo($file, PATHINFO_EXTENSION);
        $data = file_get_contents($file);
        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }
}


function excerpt($id, $length = 10)
{
    $content_post = get_post($id);
    $content = $content_post->post_content;

    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $content = strip_tags($content);
    $content = preg_replace("/\r|\n/", "", $content);

    preg_match("/(?:\w+(?:\W+|$)){0,$length}/", $content, $matches);
    echo $matches[0] . '...';
}


function jquery_footer()
{
    wp_deregister_script('jquery');
    wp_register_script('jquery', includes_url('/js/jquery/jquery.js'), false, null, false);
    wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'jquery_footer');


/**
 * Deferring JS
 */
if (!(is_admin() )) {
    function defer_parsing_of_js($url)
    {
        if (false === strpos($url, '.js')) {
            return $url;
        }
        if (strpos($url, 'jquery.js')) {
            return $url;
        }

        if (strpos($url, 'cookieControl')) {
            return $url;
        }
        // return "$url' defer ";
        return "$url' defer onload='";
    }
    //add_filter('clean_url', 'defer_parsing_of_js', 11, 1);
}


/**
 * Dequeue jQuery Migrate script in WordPress.
 */
function sst_remove_jquery_migrate(&$scripts)
{
    if (!is_admin()) {
        $scripts->remove('jquery');
        $scripts->add('jquery', false, array( 'jquery-core' ), '1.12.4');
    }
}
add_filter('wp_default_scripts', 'sst_remove_jquery_migrate');

/**
 * Numbered page navigation
 */
function numeric_posts_nav($query = null)
{
    if (is_singular()) {
        return;
    }

    if (!$query) {
        global $wp_query;
    } else {
        $wp_query = $query;
    }

    /** Stop execution if there's only 1 page */
    if ($wp_query->max_num_pages <= 1) {
        return;
    }

    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $max   = intval($wp_query->max_num_pages);

    /** Add current page to the array */
    if ($paged >= 1) {
        $links[] = $paged;
    }

    /** Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (( $paged + 2 ) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="pagination"><ul>' . "\n";

    /** Previous Post Link */
    if (get_previous_posts_link()) {
        printf('<li class="prev">%s</li>' . "\n", get_previous_posts_link('<svg aria-hidden="true" data-prefix="fas" data-icon="chevron-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-left fa-w-10 fa-2x"><path fill="currentColor" d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" class=""></path></svg>'));
    }

    /** Link to first page, plus ellipses if necessary */
    if (! in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');

        if (! in_array(2, $links)) {
            echo '<li class="ellipsis">…</li>';
        }
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort($links);
    foreach ((array) $links as $link) {
        $class = $paged == $link ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    /** Link to last page, plus ellipses if necessary */
    if (! in_array($max, $links)) {
        if (! in_array($max - 1, $links)) {
            echo '<li class="ellipsis">…</li>' . "\n";
        }

        $class = $paged == $max ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
    }

    /** Next Post Link */
    if (get_next_posts_link()) {
        printf('<li class="next">%s</li>' . "\n", get_next_posts_link('<svg aria-hidden="true" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-2x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg>'));
    }

    echo '</ul></div>' . "\n";
}


/**
 * Post Tracking for popular posts
 */
function set_post_views($postID)
{
    $count_key = 'post_views';
    $count = get_post_meta($postID, $count_key, true);
    if ($count=='') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function track_post_views($post_id)
{
    if (!is_single()) {
        return;
    }
    if (empty($post_id)) {
        global $post;
        $post_id = $post->ID;
    }
    set_post_views($post_id);
} add_action('wp_head', 'track_post_views');



function file_name()
{
    return $_GET['file_name'];
} add_shortcode('file-name', 'file_name');

function file_url()
{
    return '<a href="' . $_GET['file_url'] . '">' . $_GET['file_name'] . ' </a>';
} add_shortcode('file-url', 'file_url');

/**
 * Disable the emoji's
 */
function disable_emojis()
{
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    add_filter('tiny_mce_plugins', 'disable_emojis_tinymce');
    add_filter('wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2);
}
add_action('init', 'disable_emojis');

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array( 'wpemoji' ));
    } else {
        return array();
    }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch($urls, $relation_type)
{
    if ('dns-prefetch' == $relation_type) {
        /** This filter is documented in wp-includes/formatting.php */
        $emoji_svg_url = apply_filters('emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/');

        $urls = array_diff($urls, array( $emoji_svg_url ));
    }

    return $urls;
}

function sst_woo_start()
{
    get_template_part('template-parts/header');
    echo '<section class="shop"><div class="container">';
} add_action('woocommerce_before_main_content', 'sst_woo_start', 10);

function sst_woo_end()
{
    echo '</div></section>';
} add_action('woocommerce_after_main_content', 'sst_woo_end', 10);

function sst_acf_google_map_api($api)
{
    //$api['key'] = 'AIzaSyD37SjEQ99SUYeb_XYcSaZmdQQEQeDzB4k';
    $api['key'] = 'AIzaSyDC9JP552Ao367VSsNytqfoa7jixhslb0M';
    return $api;
} add_filter('acf/fields/google_map/api', 'sst_acf_google_map_api');


/**
 * Add to the Rest API the Featured Image URL & the Full image URL
 *
 * @return void
 */
function sureset_register_rest_images()
{
    register_rest_field(
        array('galleries'),
        'fimg_url',
        array(
            'get_callback'    => 'sureset_get_rest_featured_image',
            'update_callback' => null,
            'schema'          => null,
        )
    );

    register_rest_field(
        array('galleries'),
        'full_image_url',
        array(
            'get_callback'    => 'sureset_get_rest_full_featured_image',
            'update_callback' => null,
            'schema'          => null,
        )
    );
} add_action('rest_api_init', 'sureset_register_rest_images');


/**
 *
 *
 * @param [type] $object
 * @param [type] $field_name
 * @param [type] $request
 * @return void
 */
function sureset_get_rest_featured_image($object, $field_name, $request)
{
    if ($object['featured_media']) {
        $img = wp_get_attachment_image_src($object['featured_media'], 'medium-square');
        return $img[0];
    }
    return false;
}

/**
 *
 *
 * @param [type] $object
 * @param [type] $field_name
 * @param [type] $request
 * @return void
 */
function sureset_get_rest_full_featured_image($object, $field_name, $request)
{
    if ($object['featured_media']) {
        $img = wp_get_attachment_image_src($object['featured_media'], 'full');
        return $img[0];
    }
    return false;
}



/**
 * Function to create and add to the Google map each marker point
 *
 * @param integer $post_id        The post ID, used as an identifier
 * @param float   $lat            The location's latitude
 * @param float   $lng            The location's longitude
 * @param string  $image          The image URL
 * @param string  $name           The location's name
 * @param string  $postcode       Optional, the location's postcode
 * @param string  $case_study_url Optional, the location's case study URL
 * @return void
 */
function sureset_create_marker_script_for_projects_map($post_id, $lat, $lng, $image, $name, $postcode = '', $case_study_url = '')
{
    $stringscript = "var latlng" . $post_id . " = {lat: " . $lat . ", lng: " . $lng . "};
		var contentString" . $post_id . " = '<div style=\"padding-bottom:10px\">'+
		'<img class=\'lozad\' data-src=";
    $stringscript .= '"' . $image . '"><h1 style="font-weight: bold">'. $name . '</h1>';
    $stringscript .= "';";
    if ($postcode) {
        $stringscript .= "contentString" . $post_id . " += '<small>" . $postcode . "></small>";
    }
    if ($case_study_url) {
        $stringscript .= "contentString" . $post_id . " += '<p><a href=";
        $stringscript .= '"' . $case_study_url .'" style="color: #a32638">Read Case Study</a>';
        $stringscript .= "';";
    }

    $stringscript .= "contentString" . $post_id . " += '</div>';";
    $stringscript .= "var infowindow" . $post_id ." = new google.maps.InfoWindow({
          content: contentString" . $post_id ."
		});
		var marker" . $post_id ." = new google.maps.Marker({
          position: latlng" . $post_id . ",
          map: map,";
    $stringscript .= "title: '" . $name . "'";
    $stringscript .= "});
		marker" . $post_id . ".addListener('click', function() {
          infowindow" . $post_id . ".open(map, marker" . $post_id .");
		});";

    return $stringscript;
}

function sureset_area_calculator()
{
    ?>
    <div class="area-calculator">
        <h3>Area Calculator</h3>
        <div class="area-calculator__inner">
            <div class="calculator-row">
                <div class="calculator-item">
                    <span>Shape</span>
                    <label>
                        <select name="shape" class="shape" style="width: 304px;">
                            <option value="square">
                                Square
                            </option>
                            <option value="circle">
                                Circle
                            </option>
                            <option value="triangle">
                                Right Angled Triangle
                            </option>
                        </select>
                    </label>
                </div>
            </div>
            <div class="calculator-row square" style="">
                <div class="calculator-item ">
                    <span>Length (m)</span>
                    <div class="input"><input class="squ_length" type="number"></div>
                </div>
                <div class="calculator-item ">
                    <span>Width (m)</span>
                    <div class="input"><input class="squ_width" type="number"></div>
                </div>
                <div class="calculator-item ">
                    <span>Total Area (m²)</span>
                    <div class="input"><input class="squ_total" type="number" readonly="readonly" ></div>
                </div>
            </div>
            <div class="calculator-row circle" style="display: none">
                <div class="calculator-item ">
                    <span>Diameter (m)</span>
                    <div class="input">
                        <input class="cir_diameter" type="number">
                    </div>
                </div>
                <div class="calculator-item ">
                    <span>Circumference</span>
                    <div class="input">
                        <input class="cir_circumference" type="number">
                    </div>
                </div>
                <div class="calculator-item ">
                    <span>Total Area (m²)</span>
                    <div class="input">
                        <input class="cir_total" type="number" readonly="readonly">
                    </div>
                </div>
            </div>
            <div class="calculator-row triangle" style="display: none">
                <div class="calculator-item ">
                    <span>Height (m)</span>
                    <div class="input">
                        <input class="tri_height" type="number">
                    </div>
                </div>
                <div class="calculator-item ">
                    <span>Width (m)</span>
                    <div class="input"><input class="tri_width" type="number"></div>
                </div>
                <div class="calculator-item ">
                    <span>Total Area (m²)</span>
                    <div class="input"><input class="tri_total" type="number" readonly="readonly"></div>
                </div>
            </div>
            <div class="calculator-row">
                <div class="calculator-item">
                    <span>Type of Job</span>
                    <label>
                        <select class="type" style="width: 146px;">
                            <option value="0">
                                Straight Driveway
                            </option>
                            <option value="1">
                                Turning Driveway
                            </option>
                            <option value="2">
                                Footpath
                            </option>
                            <option value="3">
                                Patio
                            </option>
                        </select>
                    </label>
                </div>
                <div class="calculator-item">
                    <span>Base Material</span>
                    <label>
                        <select class="base" style="width: 146px;">
                            <option value="0">
                                Asphalt
                            </option>
                            <option value="1">
                                Concrete
                            </option>
                            <option value="2">
                                SureCell
                            </option>
                            <option disabled="true" value="3">
                                Type 1
                            </option>
                            <option disabled="true" value="4">
                                Type 3
                            </option>
                        </select>
                    </label>
                </div>

            </div>
            <div class="calculator-row">
                <div class="calculator-item ">
                    <div class="result trade-packs">
                        <div class="label">Number of Trade Packs</div>
                        <span>0</span>
                    </div>
                </div>
                <div class="calculator-item ">
                    <div class="result diy-kits">
                        <div class="label">Number of DIY Kits</div>
                        <span>0</span>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" class="button-red clear-form">Clear Form</a>
        <div class="calculator-row quote-me">
            <a href="/contact/" class="button-red">Quote me for full installation</a>
        </div>
    </div>
    <script type="text/javascript">

        jQuery(function($){

            var squ_area = 0,
                cir_area = 0,
                tri_area = 0,
                active = 'square',
                depth_table = [
                    [16, 18, 16, 16],
                    [16, 18, 16, 16],
                    [24, 24, 20, 20],
                    [0, 0, 30, 30],
                    [0, 0, 30, 30]
                ],
                area_table = [
                    [30, 27, 30, 30],
                    [30, 27, 30, 30],
                    [20, 20, 24, 24],
                    [0, 0, 16, 16],
                    [0, 0, 16, 16]
                ]
            base = 0, type = 0, depth = depth_table[base][type], areas = area_table[base][type], trade_packs = 0, diy_kits = 0;

            function calculateArea(shape, x, y) {
                if (shape == 'square') {
                    return x * y;
                }
                if (shape == 'circle_dia') {
                    return Math.round((Math.PI * (x / 2) * (x / 2)) * 100) / 100;
                }
                if (shape == 'circle_cir') {
                    return +Math.fround((x * x) / (Math.PI * 4)).toFixed(1);
                }
                if (shape == 'triangle') {
                    return 0.5 * x * y;
                }
            };

            function updateCalculation() {
                var area = 0;
                switch (active) {
                    case 'square':
                        area = squ_area;
                        $('.squ_total').val(area);
                        break;
                    case 'circle':
                        area = cir_area;
                        $('.cir_total').val(area);
                        break;
                    case 'triangle':
                        area = tri_area;
                        $('.tri_total').val(area);
                        break;
                }
                diy_kits = Math.ceil(area * 2 / 16 * depth);
                trade_packs = Math.ceil(area / 30 / 16 * depth);
                trade_packs_display = trade_packs;
                diy_kits_display = diy_kits;

                if( (areas * trade_packs) >= 120 || (diy_kits * .5) >= 30 ) {

                    $('.quote-me').css('display', 'block');

                }

                $('.trade-packs span').html(trade_packs_display + " <small>This covers: " + (areas * trade_packs) + "m<sup>2</sup></small>");


                if (area >= 30) {
                    $('.diy-kits span').html(0 + " <small>This covers: " + (diy_kits * .5) + "m<sup>2</sup></small>");
                } else {
                    $('.diy-kits span').html(diy_kits_display + " <small>This covers: " + (diy_kits * .5) + "m<sup>2</sup></small>");
                }

            }

            function updateTypes( type ){

                if( type == 2 || type == 3 ){
                    $('select.base option[value=3]')[0].disabled = false;
                    $('select.base option[value=4]')[0].disabled = false;
                }else{
                    $('select.base option[value=3]')[0].disabled = true;
                    $('select.base option[value=4]')[0].disabled = true;
                }

            }


            $('.' + $('.shape').val()).show();
            $('.shape').on('change', function() {
                $('.square').hide();
                $('.circle').hide();
                $('.triangle').hide();
                $('.' + $(this).val()).fadeIn(250);
                active = $(this).val();
            });
            $('.calculator-row.square input').on('change', function() {
                squ_area = calculateArea('square', $('.squ_length').val(), $('.squ_width').val());
                updateCalculation();
            });
            $('.calculator-row.circle input').on('change', function() {
                if ($(this).hasClass('cir_diameter')) {
                    cir_area = calculateArea('circle_dia', $(this).val(), 0);
                } else {
                    cir_area = calculateArea('circle_cir', $(this).val(), 0);
                }
                updateCalculation();
            });
            $('.calculator-row.triangle input').on('change', function() {
                tri_area = calculateArea('triangle', $('.tri_height').val(), $('.tri_width').val());
                updateCalculation();
            });
            $('.calculator-item .base').on('change', function() {
                base = $(this).val();
                depth = depth_table[base][type];
                areas = area_table[base][type];
                updateCalculation();
            });
            $('.calculator-item .type').on('change', function() {
                type = $(this).val();
                depth = depth_table[base][type];
                areas = area_table[base][type];
                updateCalculation();
                updateTypes( type );
            });
            $('.clear-form').click(function(){
                $('.squ_length, .squ_width, .cir_diameter, .cir_circumference, .tri_height, .tri_width').val(0);
                squ_area = 0;
                cir_area = 0;
                tri_area = 0;
                updateCalculation();
            });
        });
    </script>

    <?php
} add_shortcode('area-calculator', 'sureset_area_calculator');



remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
add_action('woocommerce_after_shop_loop_item', 'sureset_view_product_button', 10);

function sureset_view_product_button()
{
    global $product;
    $link = $product->get_permalink();
    echo '<a href="' . $link . '" class="button addtocartbutton">View Product</a>';
}

add_action('wp_footer', 'woocommerce_review_schema');

function woocommerce_review_schema()
{

    if (is_product() && have_comments()) :
        global $product;

        $average      = $product->get_average_rating();
        $rating_count = $product->get_rating_count();

        $args = array(
            'walker'            => null,
            'max_depth'         => '1',
            'style'             => 'ol',
            'callback'          => 'woocommerce_comments_schema',
            'end-callback'      => 'woocommerce_comments_schema_close',
            'type'              => 'all',
        );
        ?>

    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "<?php echo $average; ?>",
            "ratingCount": "<?php echo $rating_count; ?>"
        },
        "review": [
           <?php wp_list_comments($args); ?>
        ]
    }
    </script>
        <?php
    endif;
};

function woocommerce_comments_schema()
{

    get_template_part('woocommerce/single-product/comment_schema');
}

function woocommerce_comments_schema_close()
{
    echo ' ';
}

// Add Author column to WooCommerce so can change where review notifications go to

add_action('init', 'function_to_add_author_woocommerce', 30);

function function_to_add_author_woocommerce()
{
    add_post_type_support('product', 'author');
}




/**
 * @snippet       Move product tabs beside the product image - WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=393
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 2.5.2
 */

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 60);

add_action('woocommerce_before_account_navigation', function () {
    echo '<p><a href="/shop/">&lt;&lt; Back to Shop</a></p>';
}, 2);



/**
 * Register a sidebar for the blog archive & the newsletter
 *
 * @return void
 */
function sureset_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Newsletter Sidebar', 'textdomain' ),
        'id'            => 'sidebar-newsletter',
        'description'   => __( 'Widgets in this area will be shown on the blog archive, newsletter archive & the individual newsletters.', 'sureset' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Shop Home Sidebar', 'textdomain' ),
        'id'            => 'sidebar-shop',
        'description'   => __( 'Widgets in this area will be shown on the Shop Sidebar.', 'sureset' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'sureset_widgets_init' );


//Hide categories from WordPress category widget
function sureset_exclude_woocommerce_widget_product_categories($widget_args) {

    wp_die( print_r( $widget_args ) );
    //Insert excluded category ids here
    $excludes = array('505');
    $includes = explode(",",$widget_args['include']);

    $includes = array_filter($includes, function($value) use ($excludes) {
      return !in_array($value, $excludes);
    });
    $widget_args["include"] = implode(",", $includes);
    return $widget_args;
}
add_filter( 'woocommerce_product_categories_widget_dropdown_args', 'sureset_exclude_woocommerce_widget_product_categories');
add_filter( 'woocommerce_product_categories_widget_args', 'sureset_exclude_woocommerce_widget_product_categories');


/**
 * Show products only of selected category.
 */
function sureset_get_subcategory_terms( $terms, $taxonomies, $args ) {
 
	$new_terms 	= array();
	$hide_category 	= array( 505 ); // Ids of the category you don't want to display on the shop page
 	
 	  // if a product category and on the shop page
	if ( in_array( 'product_cat', $taxonomies ) && !is_admin() && is_shop() ) {

	    foreach ( $terms as $key => $term ) {

		if ( ! in_array( $term->term_id, $hide_category ) ) { 
			$new_terms[] = $term;
		}
	    }
	    $terms = $new_terms;
	}
  return $terms;
}
add_filter( 'get_terms', 'sureset_get_subcategory_terms', 10, 3 );