<section class="content-sidebar">
    <div class="container">
        <?php if (!has_post_thumbnail()) : ?>
            <h1><?php the_title() ?></h1>
        <?php endif; ?>

        <?php $feature_subtitle = get_field('feature_subtitle'); ?>
        <?php if ($feature_subtitle) : ?>
        <div class="feature-subtitle">
            <p><?php echo $feature_subtitle ?></p>
        </div>
        <?php endif; ?>
        <article class="content case-studies">
            <?php the_content() ?>
        </article>

        <aside>
            <?php $sidebar_images = get_field('sidebar_images');

            if ($sidebar_images) :
                foreach ($sidebar_images as $sidebar_image) :
                    error_log(print_r($sidebar_images, 1)); ?>
                <img class="lozad" data-src="<?php image($sidebar_image['sizes']['blog-sidebar']) ?>" alt="<?php $sidebar_image['alt'] ?>">
                <?php endforeach;
            endif; ?>
        </aside>
    </div>
</section>