<?php 

$link = get_field('center_button');

if( $link ): ?>
    <section class="center-button">
        <a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
    </section>
<?php endif;