<section class="home-app-download lozad" data-background-image="<?php echo theme_images('mobile-app-background.jpg') ?>">
    <div class="popper"></div>
    <div class="container">
        <div class="content">
            <p><?php the_field('app_cta_content'); ?></p>
            <a href="https://play.google.com/store/apps/details?id=com.AppInstitute.joniwith&hl=en_GB" target="_blank">
                <img class="lozad" data-src="<?php theme_images('google-badge.png') ?>" alt="SureSet Google App">
            </a>
            <a href="https://itunes.apple.com/gb/app/permeable-paving-by-sureset/id1124380934?mt=8" target="_blank">
                <img class="lozad" data-src="<?php theme_svg('badge-download-on-the-app-store.svg') ?>" alt="SureSet iPhone App">
            </a>
        </div>
        <div class="image">
            <img class="lozad" data-src="<?php theme_images('mobile-app.png') ?>" alt="SureSet App">
        </div>
    </div>
</section>