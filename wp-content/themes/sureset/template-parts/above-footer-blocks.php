<?php
    
    $left_column = get_field('left_block');
    $right_column = get_field('right_block');
    
    // https://support.advancedcustomfields.com/forums/topic/acf-fields-not-displaying-on-posts-page/
    if (is_home()) {
        $left_column = get_field('left_block', get_option('page_for_posts'));
        $right_column = get_field('right_block', get_option('page_for_posts'));
    } else if (is_shop()) {
        $left_column = get_field('left_block', get_option( 'woocommerce_shop_page_id' ));
        $right_column = get_field('right_block', get_option( 'woocommerce_shop_page_id' ));
    }
    
    if ($left_column != '') {
        ?>
        <div class="above-footer-blocks">
            <div class="container">
                <div class="left-block"><?= $left_column; ?></div>
                <?php
                if ($right_column != '') {
                    ?>
                    <div class="right-block"><?= $right_column; ?></div>
                    <?php
                }
            ?>
            </div>
        </div>
    <?php
    }