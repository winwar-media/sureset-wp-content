<?php
$featured_img_url = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(), 'medium-square') : get_template_directory_uri() . '/assets/img/larger-square.png'; ?>
<article class="feature-post">
    <img class="featured lozad" data-src="<?php echo $featured_img_url ?>" alt="<?php the_title() ?>">
    <h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
    <p class="date"><?php the_date('jS F Y') ?></p>
    <p class="subtitle"><?php excerpt(get_the_ID()); ?></p>
    <div class="button-wrap">
        <a href="<?php the_permalink() ?>" class="button-red small">Read More</a>
    </div>
    <div class="author">
        <div class="image">
            <?php echo get_avatar(get_the_author_meta('ID'), 30); ?>
        </div>
        <p>By <?php the_author() ?></p>
    </div>
</article>