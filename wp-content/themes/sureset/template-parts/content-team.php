<section class="content-sidebar">
    <div class="container">

        <?php if (!has_post_thumbnail()) : ?>
            <h1><?php the_title() ?></h1>
        <?php endif; ?>
        <article class="content">
            <?php the_content() ?>

        <?php if (have_rows('team')) :
            $teams = array( 'Senior Management',
                            'Sales Team',
                            'Marketing Team',
                            'Global Team',
                            'Operations Team',
                            'Technical and R&D Team',
                            'Accounts Team',
                            'Health & Safety and Admin Team');
            ?>
            <div class="team-filters">
                <p class="title">Filter: </p>
                <a href="#" data-team="all" class="active">All</a>
                <?php foreach ($teams as $t) : ?>
                <a href="#" data-team="<?php echo $t ?>"><?php echo $t ?></a>
                <?php endforeach; ?>
            </div>
            <div class="team">

                <?php while (have_rows('team')) :
                    the_row();
                    $name = get_sub_field('name');
                    $about = get_sub_field('about');
                    $position = get_sub_field('position');
                    $team = get_sub_field('team');
                    $email = get_sub_field('email');
                    $linkedin = get_sub_field('linkedin');
                    $phone = get_sub_field('phone');
                    $image = get_sub_field('image');
                    $image = $image['sizes']['thumbnail'];

                    if ($team != '') {
                        $team = implode(', ', $team);
                    }
                    ?>
                    <div class="team-item" data-team="<?php echo $team ?>">
                        <div class="image">
                            <img class="lozad" data-src="<?php echo $image ?>" alt="<?php $name ?>">
                        </div>
                        <div class="about">
                            <h2><?php echo $name ?> <small><?php echo $position ?></small></h2>
                            <?php if ($about != '') : ?>
                                <p><?php echo $about ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="contact">
                            <?php if ($phone) : ?>
                            <p>Tel: <a href="tel:<?php echo $phone ?>"><?php echo $phone ?></a></p>
                            <?php endif; ?>

                            <?php if ($email || $linkedin) : ?>
                            <p>
                                <?php if ($email) : ?>
                                <a href="mailto:<?php echo $email ?>"><svg aria-hidden="true" data-prefix="far" data-icon="envelope" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-envelope fa-w-16 fa-2x"><path fill="currentColor" d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm0 48v40.805c-22.422 18.259-58.168 46.651-134.587 106.49-16.841 13.247-50.201 45.072-73.413 44.701-23.208.375-56.579-31.459-73.413-44.701C106.18 199.465 70.425 171.067 48 152.805V112h416zM48 400V214.398c22.914 18.251 55.409 43.862 104.938 82.646 21.857 17.205 60.134 55.186 103.062 54.955 42.717.231 80.509-37.199 103.053-54.947 49.528-38.783 82.032-64.401 104.947-82.653V400H48z" class=""></path></svg></a>
                                <?php endif; ?>
                                <?php if ($linkedin) : ?>
                                <a href="<?php echo $linkedin ?>"><svg aria-hidden="true" data-prefix="fab" data-icon="linkedin-in" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-linkedin-in fa-w-14 fa-2x"><path fill="currentColor" d="M100.3 480H7.4V180.9h92.9V480zM53.8 140.1C24.1 140.1 0 115.5 0 85.8 0 56.1 24.1 32 53.8 32c29.7 0 53.8 24.1 53.8 53.8 0 29.7-24.1 54.3-53.8 54.3zM448 480h-92.7V334.4c0-34.7-.7-79.2-48.3-79.2-48.3 0-55.7 37.7-55.7 76.7V480h-92.8V180.9h89.1v40.8h1.3c12.4-23.5 42.7-48.3 87.9-48.3 94 0 111.3 61.9 111.3 142.3V480z" class=""></path></svg></a>
                                <?php endif; ?>
                            </p>
                            <?php endif; ?>
                        </div>
                    </div>

                <?php endwhile; ?>

            </div>
        <?php endif; ?>
        </article>

        <?php get_template_part('template-parts/sidebar-about') ?>

    </div>
</section>