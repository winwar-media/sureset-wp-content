<section class="hero-slider">
    <div class="slider-items">
        <?php if (have_rows('images')) : ?>
            <?php while (have_rows('images')) :
                the_row();
                $image = get_sub_field('image');?>

                <div class="slider-item lozad" data-background-image="<?php echo $image['sizes']['full-slider'] ?>">
                    <div class="content">
                        <div class="inner">
                            <p class="title"><?php the_sub_field('slide_title'); ?></p>
                            <p><?php the_sub_field('sub_title'); ?></p>
                        </div>
                    </div>
                </div>

            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>