<?php
$commercial = new WP_Query( array(
	'post_type'        => 'commercial_paving',
	'posts_per_page'   => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC',
) );
?>


<section class="paving-loop">
	<main class="container content">
		<?php if ( $commercial->have_posts() ) : ?>

			<h2>Choose your application</h2>

			<?php while ( $commercial->have_posts() ) : $commercial->the_post();?>

				<?php get_template_part('template-parts/commercial-loop-article'); ?>
				
			<?php endwhile; ?>

			<?php numeric_posts_nav(); ?>

		<?php else : ?>
			<p>There are no posts here.</p>
		<?php endif; ?>
		<?php wp_reset_query() ?>
	</main>
</section>