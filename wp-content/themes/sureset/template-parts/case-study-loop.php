<?php
$case_studies = new WP_Query( array(
	'post_type'        => 'case_studies',
	'posts_per_page'   => -1,
	'orderby'          => 'date',
	'order'            => 'DESC'
) );

?>


<section class="case-study-loop">
	<main class="container">
		<?php if ( $case_studies->have_posts() ) : ?>

			<?php while ( $case_studies->have_posts() ) : $case_studies->the_post();?>

				<?php get_template_part('template-parts/case-study-loop-article'); ?>
				
			<?php endwhile; ?>

			<?php numeric_posts_nav(); ?>

		<?php else : ?>
			<p>There are no posts here.</p>
		<?php endif; ?>
	</main>
</section>