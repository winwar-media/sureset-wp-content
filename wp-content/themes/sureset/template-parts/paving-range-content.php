<section class="paving-range-content" data-id="<?= get_the_id(); ?>">
<?php
	$sample_request_url = "/sample-request/";
	$id = get_the_id();
	if ($id == '13317' || $id == '13319') {
		$sample_request_url = "/uvr-sample-request/";
	}
	if(get_the_id() != '6706' && !get_field( 'hide_order_samples_button')): ?>
    <div class="grey-bg center">
        <a class="button-white" href="<?= $sample_request_url; ?>">Order Samples</a>
    </div>
<?php endif; ?>

    <div class="product container">
        <div class="gallery">
            <?php $images = get_field('images'); ?>

            <?php if ($images) : ?>
            <div class="products-slider">
                <div class="slider products-for">
                    <?php foreach ($images as $image) : ?>
                        <div class="products-slide">
                            <img src="<?php echo $image['sizes']['2048x2048'] ?>" alt="<?php echo $image['title'] ?>"/>
                            <p><?php echo $image['title'] ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="slider products-nav">
                    <?php foreach ($images as $image) : ?>
                        <div class="circle-thumb">
                            <img src="<?php echo $image['sizes']['thumbnail'] ?>" alt="<?php echo $image['title'] ?>"/>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php endif; ?>

            <?php $product_link = get_field('product_link'); ?>

            <?php if ($product_link) : ?>
            <h2>Shop Related Products</h2>
                <p>
                <?php foreach ($product_link as $product) : ?>
                    <a href="<?php echo get_the_permalink($product->ID) ?>"><?php echo $product->post_title ?></a><br>
                <?php endforeach; ?>
                </p>
            <?php endif; ?>

        </div>
        <div class="content">
            <h2><?php the_title(); ?></h2>
            <?php the_content() ?>

            <?php $embed = get_field('youtube_embed');
            if ($embed) {
                echo '<div class="youtube-embed">' . $embed . '</div>';
            } ?>
            <a class="button-black" href="<?= $sample_request_url; ?>">Request a Sample</a>
            <?php
                $button = get_field( 'download_button' );
                if ($button) { ?>
                    <a href="<?php echo esc_url( $button['url'] ); ?>" class="button-black" target="<?php echo esc_attr( $button['target'] ); ?>"><?php echo esc_html( $button['title'] ); ?></a>
            <?php } ?>
            <a class="button-black" href="/envisage-your-colour/">Back</a>
            
            

            <?php if (get_field('show_area_calculator')) {
                echo do_shortcode('[area-calculator]');
            } ?>
        </div>

    </div>

</section>