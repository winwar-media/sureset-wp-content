<?php
$gallery = get_field('gallery');
$sidebar_images = get_field('sidebar_images');

if ($gallery) {
    if ($sidebar_images) {
        $gallery = array_merge($gallery, $sidebar_images);
    }
}

if ($gallery) : ?>
<section class="gallery">
    <div class="container">
        <h2>Gallery</h2>
        <div class="gallery-for">
            <?php foreach ($gallery as $image) : ?>
                    <div class="slider-item lozad" data-background-image="<?php echo esc_url($image['sizes']['full-slider']); ?>">
                </div>
            <?php endforeach; ?>
        </div>
        <div class="gallery-nav">
            <?php foreach ($gallery as $image) : ?>
                <div class="slider-item">
                    <img class="lozad" data-src="<?php echo esc_url($image['sizes']['medium-square']); ?>" />
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>