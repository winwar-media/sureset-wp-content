<section class="content-sidebar">
	<div class="container">
		<?php if ( !has_post_thumbnail() ) : ?>
			<h1><?php the_title() ?></h1>
		<?php endif; ?>
		<article class="content case-studies">
			<?php the_field('content') ?>
		</article>

		<aside>
			<h2>Key Features</h2>
			<div class="content">
				<?php the_field('key_features'); ?>
			</div>
			<div class="content cta">
				<?php /* <a href="/contact/" class="button-red">Contact Us</a>
				<a href="/about/file-downloads/" class="button-white-bordered">Downloads</a> */ ?>
				<a class="button-red block" href="/quote-request/">Request a Quote</a>
                <a class="button-white-bordered block" href="/sample-request/">Order Samples</a>
                <a class="button-white-bordered block" href="/brochuredownload/">Download a Brochure</a>
                <a class="button-white-bordered block" href="/envisage-your-colour/">Colour Preview</a>
                <a class="button-white-bordered block" href="/contact/">Contact us</a>
                <a class="button-grey block" href="/about/file-downloads/">All Downloads</a>
			</div>
		</aside>
	</div>
</section>