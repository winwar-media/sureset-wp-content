<?php
$featured_img_url       = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(), 'medium-square'): get_template_directory_uri() . '/assets/img/larger-square.png';
$featured_img_url_large = get_the_post_thumbnail_url(get_the_ID(), 'full');

if (get_field('show_video', get_the_ID())) {
    $link = 'href="#" data-video="https://www.youtube.com/embed/' . get_field('youtube_video_id', get_the_ID()) . '"';
} else {
    $link = ' href="' .  $featured_img_url_large . '" data-featherlight="image"';
}?>

<article class="feature-post" x>
    <a <?php echo $link ?>>
    <img class="featured lozad" data-src="<?php echo $featured_img_url ?>" alt="<?php the_title() ?>">
    <div class="title">
    <h2><?php the_title() ?></h2>
    </div>
</a>
</article>