<?php if ( has_post_thumbnail() ) : ?>
<section class="hero blog" style="background-image: url( <?php echo get_the_post_thumbnail_url(get_the_ID(), 'full-slider') ?>)">
	<div class="content">
		<div class="inner">
		<h1><?php the_title() ?></h1>
		</div>
	</div>
</section>
<?php endif; ?>