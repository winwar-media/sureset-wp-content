<?php
get_header();

$id = get_page_by_path('contact');
$phone = get_field('header_phone_number', $id);

?>
<header class="header">
    <div class="container">

        <div class="logo">
                    <a href="/"><img class="lozad" data-src="<?php theme_images('sureset-logo-new.png') ?>" alt="<?php echo bloginfo('name') ?>" width="192" height="84"></a>
        </div>
        <div class="upper">
            <?php if ($phone) : ?>
                <a class="phone" href="tel:+441985841180">+44 (0)1985 841 180</a>
            <?php endif; ?>
                <?php get_search_form('true'); ?>
                <ul>
                    <li><a href="/contact/" class="request-quote" onclick="_gaq.push(['_trackEvent', 'Enquiry Intent', 'Button Click', 'Contact Us Header']);">Contact Us</a></li>
                </ul>
            </div>
        <div class="navigation">
            <nav>
                    <?php wp_nav_menu(array(
                        'menu'   => 'Main Menu',
                        'menu_class' => 'main-menu'
                    )); ?>
                    <a href="#" class="show-menu">
                        <svg aria-hidden="true" data-prefix="fas" data-icon="bars" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-bars fa-w-14 fa-3x"><path fill="currentColor" d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z" class=""></path></svg>
                        <svg aria-hidden="true" data-prefix="fas" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" class="svg-inline--fa fa-times fa-w-11 fa-2x"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z" class=""></path></svg>
                    </a>
                            <div class="close">
                                <p>&times;</p>
                            </div>
            </nav>
        </div>
    </div>
    <div class="burger">
        <div></div>
<div></div>
<div></div>
    </div>
</header>


