<section class="our-services">
    <div class="container">
        <article class="content">
            <h2><?php the_title() ?></h2>
        </article>
    </div>
    
    <?php

	if( have_rows('circle_links') ):

 	// loop through the rows of data
    while ( have_rows('circle_links') ) : the_row(); 

    	$page_link = get_sub_field('page_link');

    ?>

         <a href="<?php echo get_permalink($page_link); ?>">
	    <div class="container service-list">
	        <div class="circle-service-wrapper">
	        <div class="circle-service">
	            <div class="circle-service-inner">
	                <?php the_sub_field('circle_content') ?>
	            </div>
	        </div>
	        </div>
	        <article class="content">
	            <?php the_sub_field('description') ?>
	        </article>
	    </div>
	    </a>

    <?php 

	endwhile;

	endif;

	?>

</section>