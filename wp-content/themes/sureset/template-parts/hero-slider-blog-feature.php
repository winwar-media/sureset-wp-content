<?php
$sticky = get_option('sticky_posts');
$featured = new WP_Query(array(
    'post_type'        => 'post',
    'posts_per_page'   => -1,
    'orderby'          => 'date',
    'order'            => 'DESC',
    'post__in'         => $sticky
)); ?>
<?php if ($featured->have_posts()) : ?>
<section class="hero-slider-blog-featured">
    <div class="slider-items">
        <?php while ($featured->have_posts()) :
            $featured->the_post();
            $image = get_the_post_thumbnail_url(get_the_ID(), 'full-slider')
            ?>
        <div class="slider-item lozad" data-background-image="<?php echo $image ?>">
            <div class="popper"></div>
            <div class="content">
                <div class="inner">
                    <p class="title"><?php the_title() ?></p>
                    <a href="<?php the_permalink() ?>" class="button-white small">Read More</a>
                </div>
                <div class="meta">
                    <div class="image">
                        <?php echo get_avatar(get_the_author_meta('ID'), 35); ?>
                    </div>
                    <div class="text">
                        <p>Posted <?php the_date() ?> by <?php the_author() ?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile ?>
    </div>
</section>
<?php endif; ?>
<?php wp_reset_query(); ?>

