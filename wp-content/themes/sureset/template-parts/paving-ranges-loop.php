<?php

$args = array(

	'post_type' => 'paving_ranges',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'post__not_in' => array( '2960', '13317', '13319', '13321' )

);


// The Query
$query = new WP_Query( $args ); ?>

<?php if( $query->have_posts() ): ?>

	<section class="paving-loop paving-ranges">

	<main class="container content">

	<?php while ( $query->have_posts() ): $query->the_post();?>

		<?php get_template_part('template-parts/paving-ranges-loop-article'); ?>

	<?php endwhile;?>

	</main>

	</section>

<?php endif; ?>

<?php wp_reset_postdata(); ?>