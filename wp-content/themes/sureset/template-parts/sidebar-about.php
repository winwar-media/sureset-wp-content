<aside>
	<h2>More Information</h2>
	<?php
	$about_page = get_page_by_path( 'about' );

	$about_children = array(
		'post_type'      => 'page',
		'posts_per_page' => -1,
		'post_parent'    => $about_page->ID,
		'order'          => 'ASC',
		'orderby'        => 'menu_order'
	);


	$parent = new WP_Query( $about_children );

	if ( $parent->have_posts() ) : ?>
		<ul>
			<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
				<li><a href="<?php echo the_permalink() ?>"><?php the_title(); ?></a></li>
			<?php endwhile; ?>
		</ul>

	<?php endif; wp_reset_postdata(); ?>
</aside>