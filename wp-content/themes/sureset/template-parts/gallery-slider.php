<?php
$gallery = get_field('gallery');

if ($gallery) : ?>
<section class="gallery-slider">
    <div class="container">
        <div class="slider-items">
            <?php foreach ($gallery as $image) :
                $image = $image['sizes']['full-slider'];
                ?>
                <div class="slider-item">
                    <img class="lozad" data-src="<?php echo $image; ?>" alt="#" style="width: 100%">
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>