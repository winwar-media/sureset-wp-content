<?php 

$shop_page_id = get_option( 'woocommerce_shop_page_id' ); 

if (has_post_thumbnail( $shop_page_id )) : ?>
<section class="hero lozad" data-background-image="<?php echo get_the_post_thumbnail_url($shop_page_id, 'full-slider') ?>">
    <div class="content">
        <div class="inner">
            <h1>Sureset Shop</h1>
        </div>
    </div>
</section>
<?php endif; ?>