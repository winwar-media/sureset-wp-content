<?php if ( has_post_thumbnail()  ) :
	$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( ), 'large' );
?>


<section class="featured-image" style="background-image: url(<?php echo $featured_image[0] ?>)">
	<div class="popper"></div>
	<div class="container">
		<h1><?php the_title(); ?></h1>
	</div>
</section>
<?php endif; ?>