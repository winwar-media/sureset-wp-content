<section class="home-content">
	<div class="container content center">
		<div class="hr"></div>
		<?php the_field( 'content' ); ?>
	</div>
</section>