<section class="content-sidebar">
    <div class="container">
        <?php if (!has_post_thumbnail()) : ?>
            <h1><?php the_title() ?></h1>
        <?php endif; ?>

        <?php $feature_subtitle = get_field('feature_subtitle'); ?>
        <?php if ($feature_subtitle) : ?>
        <div class="feature-subtitle">
            <p><?php echo $feature_subtitle ?></p>
        </div>
        <?php endif; ?>
        <article class="content case-studies">
            <?php the_content() ?>
        </article>
        <?php get_sidebar('newsletter'); ?>

    </div>
</section>