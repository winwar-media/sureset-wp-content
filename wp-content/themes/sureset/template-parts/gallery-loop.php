<section class="basic-content container">
	<main class="gallery-index">
		<div class="inner">
			<?php
			$gallery = new WP_Query( array(
				'post_type'        => 'galleries',
				'posts_per_page'   => -1,
				'orderby'          => 'date',
				'order'            => 'DESC'
			) );
			?>
			<?php if ( $gallery->have_posts() ) : ?>

				<?php while ( $gallery->have_posts() ) : $gallery->the_post(); ?>

					<?php get_template_part('template-parts/gallery-loop-article'); ?>

				<?php endwhile; ?>

			<?php else : ?>
				<p>There are no posts here.</p>
			<?php endif; ?>
		</div>
		<?php wp_reset_postdata(); ?>
	</main>
</section>