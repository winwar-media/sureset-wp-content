<section class="gallery-type-filter">
	<?php $gallery_terms = get_terms( 'gallery_type' );

	if ( ! empty( $gallery_terms ) && ! is_wp_error( $gallery_terms ) ){

	?>
	<label>
		<select name="gallery_type_filter" class="gallery_type_filter">
		<option value="0">Filter...</option>

		<?php

			foreach ( $gallery_terms as $gallery_term ) {
				echo '<option value="' . $gallery_term->term_id . '">' . $gallery_term->name . '</option>';
			}

		}
		?>
		</select>
	</label>
</section>
