<section class="breadcrumbs shop">
	<div class="container">
		<div class="breadcrumb">
			<?php
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb( '','' );
			}
			?>
		</div>
		<div class="shop">
			<?php if ( is_user_logged_in() ) { ?>
			 	<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account'); ?></a>
			 <?php } 
			 else { ?>
			 	<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register'); ?>"><?php _e('Login / Register'); ?></a>
			 <?php } ?>
			 | <a href="/cart/">View Cart</a>
		</div>
	</div>
</section>