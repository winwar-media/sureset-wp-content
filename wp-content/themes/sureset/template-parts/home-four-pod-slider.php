<?php if (have_rows('feature_block_items')) : ?>
<section class="home-four-pod-slider">
    <div class="container">

        <h2><?php the_field('feature_block_title'); ?></h2>


        <div class="slider slider-four">
    <?php while (have_rows('feature_block_items')) :
        the_row(); ?>
        <?php
        $link = get_sub_field('link');
        $title = get_sub_field('title');
        $sub_title = get_sub_field('sub_title');

        if (has_post_thumbnail($link)) {
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($link), 'medium-square')[0];
        } else {
            $image = 'http://www.placehold.it/200';
        }
        ?>
            <div class="pod">
                <a href="<?php echo get_the_permalink($link) ?>">
                    <img class="lozad" data-src="<?php image($image); ?>" alt="<?php echo $title ?>">
                    <p><strong><?php echo get_the_title($link); ?></strong></p>
                    <p><?php echo $sub_title ?></p>
                </a>
            </div>
    <?php endwhile; ?>
        </div>

    </div>
</section>
<?php endif; ?>


