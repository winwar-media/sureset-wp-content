<section class="generic-content">
	<div class="container content">
		<?php if ( !has_post_thumbnail() ) : ?>
			<h1><?php the_title() ?></h1>
		<?php endif; ?>
		<?php the_content() ?>

		<h2><?php echo __('Pages', THEME_SLUG) ?></h2>
		<?php $pages = new WP_Query( array(
			'post_type'        => 'page',
			'posts_per_page'   => -1,
			'orderby'          => 'title',
			'order'            => 'ASC'
		) ); ?>
		<p>
		<?php if ( $pages->have_posts() ) : ?>
			<?php while( $pages->have_posts() ) : $pages->the_post(); ?>
				<a href="<?php the_permalink() ?>"><?php the_title() ?></a><br>
			<?php endwhile ?>
		<?php endif; wp_reset_query(); ?>
		</p>

		<h2><?php echo __('Posts', THEME_SLUG) ?></h2>
		<?php $pages = new WP_Query( array(
			'post_type'        => 'post',
			'posts_per_page'   => -1,
			'orderby'          => 'title',
			'order'            => 'ASC'
		) ); ?>
		<p>
		<?php if ( $pages->have_posts() ) : ?>
			<?php while( $pages->have_posts() ) : $pages->the_post(); ?>
				<a href="<?php the_permalink() ?>"><?php the_title() ?></a><br>
			<?php endwhile ?>
		<?php endif;  wp_reset_query(); ?>
		</p>

		<h2><?php echo __('Case Studies', THEME_SLUG) ?></h2>
		<?php $pages = new WP_Query( array(
			'post_type'        => 'case_studies',
			'posts_per_page'   => -1,
			'orderby'          => 'title',
			'order'            => 'ASC'
		) ); ?>
		<p>
		<?php if ( $pages->have_posts() ) : ?>
			<?php while( $pages->have_posts() ) : $pages->the_post(); ?>
				<a href="<?php the_permalink() ?>"><?php the_title() ?></a><br>
			<?php endwhile ?>
		<?php endif;  wp_reset_query(); ?>
		</p>

		<h2><?php echo __('Residential Paving', THEME_SLUG) ?></h2>
		<?php $pages = new WP_Query( array(
			'post_type'        => 'residential_paving',
			'posts_per_page'   => -1,
			'orderby'          => 'title',
			'order'            => 'ASC'
		) ); ?>
		<p>
		<?php if ( $pages->have_posts() ) : ?>
			<?php while( $pages->have_posts() ) : $pages->the_post(); ?>
				<a href="<?php the_permalink() ?>"><?php the_title() ?></a><br>
			<?php endwhile ?>
		<?php endif;  wp_reset_query(); ?>
		</p>

		<h2><?php echo __('Commercial Paving', THEME_SLUG) ?></h2>
		<?php $pages = new WP_Query( array(
			'post_type'        => 'residential_paving',
			'posts_per_page'   => -1,
			'orderby'          => 'title',
			'order'            => 'ASC'
		) ); ?>
		<p>
		<?php if ( $pages->have_posts() ) : ?>
			<?php while( $pages->have_posts() ) : $pages->the_post(); ?>
				<a href="<?php the_permalink() ?>"><?php the_title() ?></a><br>
			<?php endwhile ?>
		<?php endif;  wp_reset_query(); ?>
		</p>

		<h2><?php echo __('Paving Ranges', THEME_SLUG) ?></h2>
		<?php $pages = new WP_Query( array(
			'post_type'        => 'paving_ranges',
			'posts_per_page'   => -1,
			'orderby'          => 'title',
			'order'            => 'ASC'
		) ); ?>
		<p>
		<?php if ( $pages->have_posts() ) : ?>
			<?php while( $pages->have_posts() ) : $pages->the_post(); ?>
				<a href="<?php the_permalink() ?>"><?php the_title() ?></a><br>
			<?php endwhile ?>
		<?php endif;  wp_reset_query(); ?>
		</p>

		<h2><?php echo __('Products', THEME_SLUG) ?></h2>
		<?php $pages = new WP_Query( array(
			'post_type'        => 'product',
			'posts_per_page'   => -1,
			'orderby'          => 'title',
			'order'            => 'ASC'
		) ); ?>
		<p>
		<?php if ( $pages->have_posts() ) : ?>
			<?php while( $pages->have_posts() ) : $pages->the_post(); ?>
				<a href="<?php the_permalink() ?>"><?php the_title() ?></a><br>
			<?php endwhile ?>
		<?php endif;  wp_reset_query(); ?>
		</p>


	</div>
</section>