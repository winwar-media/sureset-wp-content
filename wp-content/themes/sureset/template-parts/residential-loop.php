<?php
$residential = new WP_Query( array(
	'post_type'        => 'residential_paving',
	'posts_per_page'   => -1,
	'orderby' => 'menu_order',
	'order' => 'ASC',
) );
?>


<section class="paving-loop">
	<main class="container content">
		<?php if ( $residential->have_posts() ) : ?>

			<h2>Choose your application</h2>

			<?php while ( $residential->have_posts() ) : $residential->the_post();?>

				<?php get_template_part('template-parts/residential-loop-article'); ?>

			<?php endwhile; ?>

			<?php numeric_posts_nav(); ?>

		<?php else : ?>
			<p>There are no posts here.</p>
		<?php endif; ?>
		<?php wp_reset_query() ?>
	</main>
</section>