<section class="home-logo-bar">
    <div class="container">
        <?php
        if (is_front_page()) {
            $logos = get_field('footer_logos');
        } else {
            $home = get_option('page_on_front');
            $logos = get_field('footer_logos', $home);
        }
        $x = 0;
        ?>
        <?php foreach ($logos as $logo) :  ?>
            <img class="lozad" data-src="<?php echo $logo['url']; ?>" alt="" width="<?= $logo['width']; ?>" height="<?= $logo['height']; ?>">
            <?php

            $x++;

            echo '<!--' . $x . '-->';

            if ( $x == 6 ) {
                echo '<br/>';
            }
            ?>
        <?php endforeach; ?>
        <a href='https://www.specifiedby.com/sureset-resin-systems'><img src='https://specifiedbypro.objects.frb.io/images/recommended-on-specifiedby-star-gold.png' alt='gold badge image' width="200" height="60" /></a>
    </div>
</section>