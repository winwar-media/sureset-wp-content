<?php
    $image = get_field('video_background');
if (get_field('youtube_video_id') && $image ) {
    ?>
    <section class="video-block lozad" data-background-image="<?php echo $image['sizes']['full-slider'] ?>">
    <div class="inner">
        <h2><?php the_field('video_title') ?></h2>
    <?php
        $youtube_id = get_field('youtube_video_id');

    if (strpos($youtube_id, 'list') !== false) : ?>
        <a href="#" data-video="https://www.youtube.com/embed/videoseries?<?php echo $youtube_id; ?>" data-playlist="true" frameborder="0" allow="autoplay; encrypted-media">
    <?php else : ?>
        <a href="#" data-video="https://www.youtube.com/embed/<?php echo $youtube_id; ?>" data-playlist="null" frameborder="0" allow="autoplay; encrypted-media">
    <?php endif; ?>

        <svg aria-hidden="true" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-2x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg>
        </a>
        <p>Watch Video</p>
    </div>
</section>
<?php } ?>