<section class="basic-content container">
	<main class="blog-index">
		<h1>Blog Posts</h1>


		<div class="inner">
			<?php

			$newsletter = new WP_Query(
				array(
					'posts_per_page' => '1',
					'post_type' => 'newsletter'
				)
			);

			wp_reset_postdata();

			if ( $newsletter->have_posts() ) { ?>

				<div class="posts-inner">
					<?php if ( have_posts() ) : ?>

						<?php while ( have_posts() ) : the_post();?>

							<?php get_template_part('template-parts/posts-loop-article'); ?>

						<?php endwhile; ?>

						<?php numeric_posts_nav(); ?>

					<?php else : ?>
						<p>There are no posts here.</p>
					<?php endif; ?>

				</div>
				<?php get_sidebar('newsletter'); ?>

			<?php } else {

				if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post();?>

						<?php get_template_part('template-parts/posts-loop-article'); ?>

					<?php endwhile; ?>

					<?php numeric_posts_nav(); ?>

				<?php else : ?>
					<p>There are no posts here.</p>
				<?php endif; ?>


			<?php } ?>
		</div>

	</main>
	<?php get_template_part('template-parts/popular-posts-aside') ?>
</section>