<section class="content-sidebar">
	<div class="container">

		<?php if ( !has_post_thumbnail() ) : ?>
			<h1><?php the_title() ?></h1>
		<?php endif; ?>
		<article class="content">
			<?php the_content() ?>

		<?php if( have_rows('faq') ): ?>
			<div class="faqs">

				<?php while ( have_rows('faq') ) : the_row(); ?>
					<div class="faq-item">
						<h2 class="question"><?php the_sub_field('question'); ?></h2>
						<div class="answer"> <?php the_sub_field('answer'); ?></div>
					</div>

				<?php endwhile; ?>

			</div>
			<?php endif; ?>
		</article>

		<?php get_template_part('template-parts/sidebar-about') ?>

	</div>
</section>