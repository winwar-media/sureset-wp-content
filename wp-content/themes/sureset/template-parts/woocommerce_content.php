<section class="woocommerce">
	<div class="container">
		<?php

		if (is_shop()) {
			get_template_part('template-parts/shop-sidebar');

			echo '<div class="woocommerce-main-shop-content">';
		}
		?>
		<?php woocommerce_content(); ?>
		<?php

		if (is_shop()) {
			echo '</div>';
		}

		?>
	</div>
</section>