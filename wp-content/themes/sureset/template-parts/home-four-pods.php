<?php if( have_rows('home_links') ): ?>

<section class="home-four-pods">
	<div class="container" style="max-width: 100%; flex-wrap: wrap;">
	<?php while ( have_rows('home_links') ) : the_row(); ?>
		<?php
		$link = get_sub_field('link');
		$title = get_sub_field('title');
		if(has_post_thumbnail($link)){
            $image = get_post_thumbnail_id($link);
			$image = wp_get_attachment_image_src(get_post_thumbnail_id($link), 'medium-square')[0];
		} else {
			$image = 'http://www.placehold.it/200';
		}
		?>
		<div class="pod" style="background-image: url(<?php echo $image; ?>)" onClick="window.location='<?php echo get_the_permalink($link); ?>'">
			<p><?php echo $title ?></p>
			<a class="button-red small" href="<?php echo get_the_permalink($link) ?>">Explore <svg aria-hidden="true" data-prefix="fas" data-icon="chevron-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-chevron-right fa-w-10 fa-2x"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" class=""></path></svg></a>
		</div>
	<?php endwhile; ?>

	</div>
</section>
<?php endif; ?>


