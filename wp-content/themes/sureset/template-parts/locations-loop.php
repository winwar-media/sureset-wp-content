<?php
$locations = new WP_Query( array(
	'post_type'        => 'locations',
	'posts_per_page'   => -1,
	'orderby'          => 'title',
	'order'            => 'ASC'
) );
?>


<section class="paving-loop">
	<main class="container content">
		<?php if ( $locations->have_posts() ) : ?>

			<h2>View our Locations</h2>

			<?php while ( $locations->have_posts() ) : $locations->the_post();?>

				<?php get_template_part('template-parts/locations-loop-article'); ?>
				
			<?php endwhile; ?>

			<?php numeric_posts_nav(); ?>

		<?php else : ?>
			<p>There are no posts here.</p>
		<?php endif; ?>
		<?php wp_reset_query() ?>
	</main>
</section>