<?php

$product_array = get_field('woo_cat_id');


if ( is_page( 760 ) ) {
    $product_array = array( 275, 68, 494, 524 );
}
?>


<section class="paving-loop">
    <main class="container content">
        <?php if ($product_array) : ?>
            <h2>Choose your product</h2>

            <?php foreach ($product_array as $id) :
                $thumbnail = get_woocommerce_term_meta($id, 'thumbnail_id', true);

                $term_obj = get_term_by('id', $id, 'product_cat');

                $title = $term_obj->name;

                $link = get_term_link($id, 'product_cat');

                $featured_img_url = wp_get_attachment_image_url($thumbnail, 'larger-square') ? wp_get_attachment_image_url($thumbnail, 'larger-square') : wc_placeholder_img_src();

                ?>
                <article>
                    <img  class="lozad" data-src="<?php echo $featured_img_url; ?>" alt="<?php echo esc_html($title);?>">
                    <h3><?php echo esc_html($title); ?></h3>
                    <a class="button-red" href="<?php echo esc_url($link); ?>">Read More <svg aria-hidden="true" data-prefix="fal" data-icon="eye" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-eye fa-w-18 fa-2x"><path fill="currentColor" d="M569.354 231.631C512.969 135.948 407.808 72 288 72 168.14 72 63.004 135.994 6.646 231.63a47.999 47.999 0 0 0 0 48.739C63.032 376.053 168.192 440 288 440c119.86 0 224.996-63.994 281.354-159.631a48.002 48.002 0 0 0 0-48.738zM416 228c0 68.483-57.308 124-128 124s-128-55.517-128-124 57.308-124 128-124 128 55.517 128 124zm125.784 36.123C489.837 352.277 393.865 408 288 408c-106.291 0-202.061-56.105-253.784-143.876a16.006 16.006 0 0 1 0-16.247c29.072-49.333 73.341-90.435 127.66-115.887C140.845 158.191 128 191.568 128 228c0 85.818 71.221 156 160 156 88.77 0 160-70.178 160-156 0-36.411-12.833-69.794-33.875-96.01 53.76 25.189 98.274 66.021 127.66 115.887a16.006 16.006 0 0 1-.001 16.246zM224 224c0-10.897 2.727-21.156 7.53-30.137v.02c0 14.554 11.799 26.353 26.353 26.353 14.554 0 26.353-11.799 26.353-26.353s-11.799-26.353-26.353-26.353h-.02c8.981-4.803 19.24-7.53 30.137-7.53 35.346 0 64 28.654 64 64s-28.654 64-64 64-64-28.654-64-64z" class=""></path></svg></a>
                </article>
            <?php endforeach; ?>

        <?php endif; ?>
    </main>
</section>