<section class="author-block">
	<div class="container">
		<div class="content">
			<p>Category: <?php echo get_the_category_list(', '); ?></p>
			<p>Posted <?php the_date() ?> by <?php the_author() ?></p>
			<p><?php the_author_meta('description') ?></p>
		</div>
		<div class="avatar">
			<?php echo get_avatar( get_the_author_meta( 'ID' ), 100 ); ?>
		</div>
	</div>
</section>