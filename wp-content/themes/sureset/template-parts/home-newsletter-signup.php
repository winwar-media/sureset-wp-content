<section class="home-newsletter-signup">
	<div class="container">
		<h2>Sign up for our newsletter</h2>
		<p>Subscribe for more news, tips &amp; resources.</p>
		<form action="#">
			<input type="email" name="email" placeholder="Email Address"/>
			<button type="submit" class="button-red">Sign Up</button>
		</form>
	</div>
</section>