<section class="content-sidebar">
    <div class="container">

        <?php if (!has_post_thumbnail()) : ?>
            <h1><?php the_title() ?></h1>
        <?php endif; ?>
        <article class="content">
            <?php the_content() ?>

            <div class="testimonial">
                <?php $testimonial = get_field('selected_testimonial'); ?>
                <p><strong>TESTIMONIAL</strong></p>
                <p>&ldquo;<?php echo $testimonial->post_content ?>&rdquo; - <?php echo $testimonial->post_title ?></p>

            </div>
        </article>

        <aside class="content">
            <div class="links">
                <a class="button-red block" href="/quote-request/" onclick="_gaq.push(['_trackEvent', 'Enquiry Intent', 'Button Click', 'Request A Quote']);">Request a Quote</a>
                <a class="button-white-grey-bordered block" href="/sample-request/" onclick="_gaq.push(['_trackEvent', 'Enquiry Intent', 'Button Click', 'Order Samples']);">Order Samples</a>
                <a class="button-white-grey-bordered block" href="/brochuredownload/" onclick="_gaq.push(['_trackEvent', 'Enquiry Intent', 'Button Click', 'Download A Brochure']);">Download a Brochure</a>
                <a class="button-white-grey-bordered block" href="/envisage-your-colour/">Colour Preview</a>
                <a class="button-white-grey-bordered block" href="/contact/" onclick="_gaq.push(['_trackEvent', 'Enquiry Intent', 'Button Click', 'Contact us']);">Contact us</a>
                <a class="button-grey block" href="/about/file-downloads/">All Downloads</a>

            </div>
            <?php $products = get_field('sidebar_products');

            if ($products) : ?>
                <div class="ranges">
                    <h2 class="center">Browse our Products</h2>
                    <?php foreach ($products as $product) :
                        $thumbnail = get_the_post_thumbnail_url($product->ID, 'medium-square')
                        ?>
                        <a href="<?php echo get_permalink($product->ID) ?>">
                            <img class="lozad" data-src="<?php image($thumbnail) ?>" alt="<?php echo $product->post_title ?>">
                            <?php echo $product->post_title ?></a>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </aside>

    </div>
</section>