<section class="brochure-download">
    <div class="container">
        <img class="lozad" data-src="<?php theme_images('brochure-image.png'); ?>" alt="Brochure" width="229" height="193">
        <div class="inner">
            <h2>Download Brochure</h2>
            <a href="/about/file-downloads/" class="button-white">More Details</a>
        </div>
    </div>
</section>