<?php if (have_rows('process')) : ?>
<section class="process">
    <div class="container">
       <h2>Process</h2>
    </div>
    <div class="container">
        <div class="process-for">
            <?php while (have_rows('process')) :
                the_row();

                $title = get_sub_field('title');
                $image = get_sub_field('image');

                if ($image) {
                    $image_large = $image['url'];
                    $image = $image['sizes']['full-slider'];
                } else {
                    $image = 'http://www.placehold.it/1400x500';
                }
                ?>
                <div class="slider-item lozad" data-background-image="<?php echo esc_url($image); ?>">
                    <p><?php echo $title ?></p>
                </div>
            <?php endwhile;
            ; ?>
        </div>
        <div class="process-nav">
        <?php while (have_rows('process')) :
            the_row();

            $title = get_sub_field('title');
            $image = get_sub_field('image');

            if ($image) {
                $image = $image['sizes']['medium-square'];
            } else {
                $image = 'http://www.placehold.it/200';
            }
            ?>
        <div class="pod">
            <img class="lozad" data-src="<?php echo esc_url($image); ?>" />
            <p><?php echo $title ?></p>
        </div>
        <?php endwhile; ?>
    </div>

    </div>
    
</section>
<?php endif; ?>