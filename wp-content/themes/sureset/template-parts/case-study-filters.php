<section class="archive-filter case-study-filter">

	<div class="upper-tabs">
		<ul>
			<li><a href="#" data-tab="all" class="active">All</a></li>
			<li><a href="#" data-tab="sector">Sector <svg aria-hidden="true" data-prefix="far" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-2x"><path fill="currentColor" d="M441.9 167.3l-19.8-19.8c-4.7-4.7-12.3-4.7-17 0L224 328.2 42.9 147.5c-4.7-4.7-12.3-4.7-17 0L6.1 167.3c-4.7 4.7-4.7 12.3 0 17l209.4 209.4c4.7 4.7 12.3 4.7 17 0l209.4-209.4c4.7-4.7 4.7-12.3 0-17z" class=""></path></svg></a></li>
			<li><a href="#" data-tab="service">Services <svg aria-hidden="true" data-prefix="far" data-icon="chevron-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-chevron-down fa-w-14 fa-2x"><path fill="currentColor" d="M441.9 167.3l-19.8-19.8c-4.7-4.7-12.3-4.7-17 0L224 328.2 42.9 147.5c-4.7-4.7-12.3-4.7-17 0L6.1 167.3c-4.7 4.7-4.7 12.3 0 17l209.4 209.4c4.7 4.7 12.3 4.7 17 0l209.4-209.4c4.7-4.7 4.7-12.3 0-17z" class=""></path></svg></a></li>
		</ul>
	</div>

	<div class="container lower-tabs">
		<div class="tab-item sector">
			<div class="inner">
			<?php $sectors = get_terms( array(
				'taxonomy' => 'sector',
				'hide_empty' => false,
			) ); ?>
			<?php foreach($sectors as $sector): ?>
				<a href="#" data-sector="<?php echo $sector->slug ?>"><?php echo $sector->name ?></a>
			<?php endforeach; ?>
			</div>
		</div>
		<div class="tab-item service">
			<div class="inner">
			<?php $services = get_terms( array(
				'taxonomy' => 'service',
				'hide_empty' => false,
			) ); ?>
			<?php foreach($services as $service): ?>
				<a href="#" data-service="<?php echo $service->slug ?>"><?php echo $service->name ?></a>
			<?php endforeach; ?>
			</div>
		</div>
	</div>

</section>