<section class="basic-content container">
	<main class="blog-index">
		<h1>Newsletters</h1>
		<div class="inner">
			<div class="newsletter-inner">
				<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post();?>

						<?php get_template_part('template-parts/posts-loop-article'); ?>

					<?php endwhile; ?>

					<?php numeric_posts_nav(); ?>

				<?php else : ?>
					<p>There are no posts here.</p>
				<?php endif; ?>

			</div>
			<?php get_sidebar('newsletter'); ?>
		</div>

	</main>
	<?php //get_template_part('template-parts/popular-posts-aside')  ?>
</section>