<?php
$colours = new WP_Query(array(
    'post_type'        => 'paving_ranges',
    'posts_per_page'   => -1,
    'orderby'          => 'title',
    'order'            => 'ASC',
    'meta_key'         => 'type',
    'meta_value'       => 'Products'
));?>




<?php if ($colours->have_posts()) : ?>
    <section class="home-four-pod-slider alt-pods">
        <div class="container">
            <h2><?php the_field('product_block_title'); ?></h2>
            <div class="slider slider-four">
                <?php while ($colours->have_posts()) :
                    $colours->the_post(); ?>
                    <?php
                    $link = get_the_permalink();
                    $title = get_the_title();
                    if (has_post_thumbnail()) {
                        $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'medium-square');
                        $image = $image[0];
                    } else {
                        $image = 'http://www.placehold.it/200';
                    }
                    ?>
                    <div class="pod">

                        <a href="<?php echo $link ?>">
                            <img class="lozad" data-src="<?php image($image); ?>" alt="<?php echo $title ?>">
                            <p><strong><?php echo $title ?></strong><br></p>
                        </a>
                        <a class="read-more" href="<?php echo $link ?>">Read More</a>
                    </div>
                <?php endwhile ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php
wp_reset_query();
?>