<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<section class="generic-content">
	<div class="container content">
		<?php if ( !has_post_thumbnail() ) : ?>
			<h1><?php the_title() ?></h1>
		<?php endif; ?>
		<?php the_content() ?>
	</div>
</section>
<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>