<section class="search-shop">
	<div class="inner">


		<form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
			<label>
				<span class="screen-reader-text">Search Shop:</span>
				<input type="search" class="search-field" placeholder="Search …" value="<?php echo $search; ?>" name="s">
				<input type="hidden" class="post_type" value="product" name="post_type">
				<input type="hidden" class="search_page" value="shop" name="search_page">
			</label>
			<button type="submit" class="button-white">Search</button>
		</form>
	</div>
</section>