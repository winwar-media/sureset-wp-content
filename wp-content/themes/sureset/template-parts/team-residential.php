<?php
$team_page = '172'; // the ID of the team page.

?>
<section class="content-team">
    <div class="container">
        <?php if (have_rows('team', $team_page)) : ?>
        <article class="content team alternate">
            <h2>Our Residential Team</h2>

            <?php while (have_rows('team', $team_page)) :
                the_row();
                $show_on_page = get_sub_field('show_on_page');

                if ($show_on_page && in_array('Residential', $show_on_page)) {
                    $name = get_sub_field('name');
                    $about = get_sub_field('about');
                    $position = get_sub_field('position');
                    $team = get_sub_field('team');
                    $email = get_sub_field('email');
                    $linkedin = get_sub_field('linkedin');
                    $phone = get_sub_field('phone');
                    $image = get_sub_field('image');
                    $image = $image['sizes']['medium-square'];

                    if ($team != '') {
                        $team = implode(', ', $team);
                    }
                    ?>
                    <div class="team-item" data-team="<?php echo $team ?>">
                        <div class="image">
                            <img class="lozad" data-src="<?php echo $image ?>" alt="<?php $name ?>">
                        </div>
                        <div class="about">
                            <h3><?php echo $name ?></h3>
                            <p><?php echo $position ?></p>
                        </div>
                    </div>
                <?php } ?>

            <?php endwhile; ?>
        </article>
        <?php endif; ?>
    </div>
</section>
