<?php
if (isset($_GET['postcode'])) {
    $postcode = esc_attr($_GET['postcode']);
    $postcodestring = $postcode;
} else {
    $postcode = 'ba127bz';
    $postcodestring = 'BA12 7BZ';
}

if (isset($postcode)) {
    $search = new Search($postcode);
    if ($search->postcode_status) {
        $locations = $search->locations_ordered;
        $locations = array_slice($locations, 0, 5);
        $error = false;
    } else {
        $error = true;
    }
}
?>

<section class="projects-search">
    <div class="container">
        <div class="search">
            <h1><?php the_title() ?></h1>
            <p>Showing <?php echo sizeof($locations); ?> nearest results to <?php echo strtoupper($postcodestring); ?>. Please input full postcode in the search box</p>

            <form action="./" action="get">
                <input type="text" name="postcode" value="<?php $postcodestring; ?>" placeholder="Enter full postcode" />
                <button type="submit">Search</button>
            </form>
        </div>
    </div>
</section>
<section class="projects">
    <div class="container">
        <div class="results-wrap">
            <div class="map">
                <div id="map" style="height: 400px; width: 100%;"></div>

                <div class="results">

                    <?php

                    $stringscript = '';

                    foreach ($locations as $location) :
                        $address    = get_field('address', $location['post_id']);
                        $place      = get_field('location', $location['post_id']);
                        $country    = get_field('country', $location['post_id']);
                        $colours    = get_field('colours', $location['post_id']);
                        $products   = get_field('products', $location['post_id']);
                        $case_study = get_field('case_study', $location['post_id']);
                        $directions = get_field('directions', $location['post_id']);
                        $thumbnailurl = get_the_post_thumbnail_url($location['post_id'], 'medium-square');

                        ?>
                        <div class="result">
                            <div class="distance"><?php echo $location['distance']; ?> Miles</div>
                            <img class="lozad" data-src="<?php echo $thumbnailurl; ?>" alt="">
                            <h2><?php echo get_the_title($location['post_id']) ?></h2>
                            
                            <p><?php echo $place; ?>, <?php echo $country; ?></p>
                            <?php if ($colours) : ?>
                            <p>Colour: <?php echo $colours ?></p>
                            <?php endif; ?>
                            <?php if ($products) : ?>
                            <p>Products: <?php echo $products ?></p>
                            <?php endif; ?>
                            <?php if ($directions) : ?>
                            <p><a href="<?php echo $directions; ?>">Get Directions</a></p>
                            <?php endif; ?>
                            <?php if ($case_study) : ?>
                            <p><a href="<?php echo $case_study; ?>">Read Case Study</a></p>
                            <?php endif; ?>
                        </div>
                        <?php
                            $stringscripttemp = sureset_create_marker_script_for_projects_map($location['post_id'], $address['lat'], $address['lng'], $thumbnailurl, get_the_title($location['post_id']), '', $case_study);
                            $stringscript .= $stringscripttemp;
                        ?>
                    <?php endforeach; ?>

                </div>

            </div>
        </div>

    </div>
    <pre>
</pre>
    <script>
        function initMap() {
            var user_area = {lat: <?php echo $search->location['result']['latitude'] ?>, lng: <?php echo $search->location['result']['longitude'] ?>};
            var map = new google.maps.Map(
                            document.getElementById('map'), {zoom: 8, center: user_area});
            <?php echo $stringscript; ?>
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC9JP552Ao367VSsNytqfoa7jixhslb0M&callback=initMap">
    </script>

</section>
