
<section class="services-layout">
    <div class="container">
        <div class="service-sub-title">
        <h2><?php the_title(); ?></h2>
        </div>
    </div>
	<div class="container">
        <article class="content">
            <?php the_content(); ?>
        </article>
        <div class="sidebar">
            <a class="button-red" href="/contact/" onclick="ga('send', 'event', 'Enquiry Intent', 'Button Click', 'Request A Quote');">Request a Quote</a>
            <a class="button-clear" href="/contact/" onclick="ga('send', 'event', 'Enquiry Intent', 'Button Click', 'Order Samples');">Order Samples</a>
            <a class="button-clear" href="/contact/" onclick="ga('send', 'event', 'Enquiry Intent', 'Button Click', 'Request A Brochure');">Request a Brochure</a>
            <a class="button-clear" href="/envisage-your-colour/">Colour Preview</a>
            <a class="button-grey" href="/about/file-downloads/">Downloads</a>

            <?php if( get_field( 'show_area_calculator' ) ){
                echo do_shortcode('[area-calculator]');
            } ?>
        </div>
	</div>
</section>