<?php if (have_rows('gravel_slider')) : ?>
    <section class="envisage-block">
        <div class="container">
            <div class="envisage-slider">
                <div class="slider envisage-for">
                    <?php
                    while (have_rows('gravel_slider')) :
                        the_row();
                        ?>
                        <div class="envisage-slide">
                            <img class="lozad" data-src="<?php echo wp_get_attachment_image_src(get_sub_field('driveway'), 'full-slider')[0]; ?>"/>
                            <img class="small-circle lozad" data-src="<?php echo wp_get_attachment_image_src(get_sub_field('gravel'), 'thumbnail')[0]; ?>"/>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="slider envisage-title">
                    <?php
                    while (have_rows('gravel_slider')) :
                        the_row();
                        $link = get_sub_field('link');
                        ?>
                        <div class="envisage-title-txt"><h2><a href="<?php if ($link) :
                            echo $link;
                                                                     else :
                                                                            echo '#';
                                                                     endif; ?>"><?php the_sub_field('title'); ?></a></h2></div>
                    <?php endwhile; ?>
                </div>
                <div class="slider envisage-nav">
                    <?php
                    while (have_rows('gravel_slider')) :
                        the_row();
                        ?>
                        <div class="circle-thumb">
                            <img class="lozad" data-src="<?php echo wp_get_attachment_image_src(get_sub_field('gravel'), 'thumbnail')[0]; ?>"/>
                        </div>
                    <?php endwhile; ?>
                </div>
                
            </div>
            
            <article class="content">
                <?php the_content(); ?>

                <a href="/contact/" class="button-white-bordered">Order Samples</a>
            </article>
        </div>
    </section> 
<?php endif; ?>