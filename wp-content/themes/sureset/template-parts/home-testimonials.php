<section class="home-testimonials">
    <div class="container">

        <h2>Testimonials</h2>

        <?php $testimonals = new WP_Query(array(
            'post_type'        => 'testimonials',
            'posts_per_page'   => 10,
            'orderby'          => 'date',
            'order'            => 'DESC'
        )); ?>

        <?php if ($testimonals->have_posts()) : ?>
            <div class="slider slider-one">
            <?php while ($testimonals->have_posts()) :
                $testimonals->the_post();

                if (has_post_thumbnail($post->ID)) {
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium-square');
                    $image = $image[0];
                } else {
                    $image = false;
                }
                ?>
            <div class="testimonial">
                <div class="inner">
                    <?php if ($image) : ?>
                    <img class="lozad" data-src="<?php echo $image ?>" alt="<?php the_title(); ?>">
                    <?php endif; ?>
                    <?php the_content(); ?>
                    <p class="name"><?php the_title(); ?></p>
                </div>
            </div>
            <?php endwhile ?>
            </div>

        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </div>
</section>