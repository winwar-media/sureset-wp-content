<?php if ( is_active_sidebar( 'sidebar-shop' ) ) { ?>
    <aside class="shop-sidebar">
        <ul id="sidebar">
            <?php dynamic_sidebar('sidebar-shop'); ?>
        </ul>
    </aside>
<?php } ?>