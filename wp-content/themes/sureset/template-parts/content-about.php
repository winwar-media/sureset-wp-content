<section class="content-sidebar">
	<div class="container">

		<?php if ( !has_post_thumbnail() ) : ?>
			<h1><?php the_title() ?></h1>
		<?php endif; ?>
		<article class="content">
			<?php the_content() ?>
		</article>

		<?php get_template_part('template-parts/sidebar-about') ?>

	</div>
</section>