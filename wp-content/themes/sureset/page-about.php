<?php
/** Template Name: About Template */
get_template_part('template-parts/header');

get_template_part('template-parts/breadcrumbs');

get_template_part('template-parts/featured-image');

get_template_part('template-parts/content-about');

get_template_part('template-parts/above-footer-blocks');

get_footer();